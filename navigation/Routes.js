export default {
  HOME: "Home",
  PAGE_LIST: "Page List",
  MATCHES: "Matches",
  MARKETPLACE: "Marketplace",
  PREDICTIONS: "Predictions",
  SERIES_DETAILS: "Series Details",
  SETTINGS: "Settings",
  MATCH_DETAILS: "Match Details",
  CHANGE_PASSWORD: "Change Password",
  EDIT_PROFILE: "Edit Profile",
  MORE: "More",
  DRAWER: "Drawer",
};
