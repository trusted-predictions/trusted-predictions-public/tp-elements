import React from "react";
import { Text, View } from "react-native";
import { Card } from "react-native-elements";
import { TouchableRipple } from "react-native-paper";
import { cardStyles } from "../styles/card";
import LiveIcon from "./Card/LiveIcon";
import ScoreCardItems from "./Card/ScoreCardItems";

const liveMatches = [
  {
    nameOfLeague: "Name of league",
    team1: {
      name: "Australia",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
    },
    team2: {
      name: "India",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
    },
    score: "248-2 (AUS)",
    overs: "21.5 Overs",
    id: "1",
  },
  {
    nameOfLeague: "Name of league",
    team1: {
      name: "Australia",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
    },
    team2: {
      name: "India",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
    },
    score: "248-2 (AUS)",
    overs: "21.5 Overs",
    id: "2",
  },
  {
    nameOfLeague: "Name of league",
    team1: {
      name: "Australia",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
    },
    team2: {
      name: "India",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
    },
    score: "248-2 (AUS)",
    overs: "21.5 Overs",
    id: "3",
  },
];

const LiveMatches = () => {
  return (
    <Card containerStyle={[cardStyles.cardContainer, cardStyles.redBorder]}>
      {/* Card Header */}
      <View style={cardStyles.cardHeaderContainer}>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <LiveIcon />
          <Text style={cardStyles.cardTitle}>Matches</Text>
        </View>

        <TouchableRipple onPress={() => {}} style={cardStyles.cardRight}>
          <Text style={cardStyles.cardRightText}>View All</Text>
        </TouchableRipple>
      </View>
      {/* Card Items */}
      {liveMatches.map((item) => (
        <ScoreCardItems item={item} key={item.id} />
      ))}
    </Card>
  );
};

export default LiveMatches;
