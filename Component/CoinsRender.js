import React from 'react'
import { View, StyleSheet, ScrollView } from 'react-native';
import AvailBalanceRender from './CoinManagement/AvailBalanceRender';
import ReferPage from './CoinManagement/ReferPage';
import RecentTransactions from './CoinManagement/RecentTransactions';
import { Sizing } from "../styles/index";

function CoinsRender() {
    return (
        <View style={styles.Container}>
            <ScrollView showsVerticalScrollIndicator={false} >
                <View style={styles.Divider}>
                    <AvailBalanceRender />
                </View>
                <View style={styles.Divider}> 
                    <RecentTransactions />
                </View>
            </ScrollView>
        </View>
    )
}

export default CoinsRender

const styles = StyleSheet.create({
    Divider:{
        marginTop:Sizing.x20
    },
    Container:{
        marginTop:60,
    }
});