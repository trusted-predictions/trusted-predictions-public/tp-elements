import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native';
import { Sizing, Outlines, Colors, Typography, Theme } from "../../styles/index";
import { Icon } from 'react-native-elements'

function LatestNewsReel({date, topic, news, source, imageSource}) {
    return (
        <View style={styles.Container}>
            <View style={styles.imageHolder}>
                <Image source={imageSource} style={styles.bgImage}></Image>
            </View>
            <View style={styles.newsTextHolder}>
                <View style={styles.newsDate}>
                    <Text style={styles.dateText}>{date}</Text>
                    <Text style={styles.topicText}>{topic}</Text>
                </View>
                <View style={styles.newsContent}>
                    <Text style={styles.contentText}>{news}</Text>
                </View>
                <View style={styles.newsSource}>
                    <Text style={styles.sourceText}>{source}</Text>
                    <Icon  onPress={()=>{}} name="dots-three-vertical" type="entypo"  size={20} color={Theme.colors.veryLightGrey}></Icon>
                </View>
            </View>
        </View>
    )
}

export default LatestNewsReel

const styles = StyleSheet.create({

    // text styling
    sourceText:{
        flex:1,
        color:Colors.success.s400,
        ...Typography.fontWeight.semibold
    },
    contentText:{
        ...Typography.fontWeight.semibold,
        ...Typography.fontSize.x10,
        paddingTop:Sizing.x10,
    },
    topicText:{
        ...Typography.fontSize.x10,
        color:Theme.colors.error,
        ...Typography.fontWeight.semibold
    },
    dateText:{
        flex:1,
        ...Typography.fontSize.x10,
        color:Theme.colors.lightGrey,
        ...Typography.fontWeight.semibold
    },

    imageHolder:{
        marginBottom:Sizing.x20,
        marginTop:Sizing.x20,
    },
    newsDate:{
        flexDirection:'row',
    },
    newsContent:{
        flex:1
    },
    newsSource:{
        flexDirection:'row',
        alignItems:"center"
    },
    newsTextHolder:{
        marginLeft:Sizing.x10,
        marginBottom:Sizing.x20,
        marginTop:Sizing.x20,
        width:Sizing.x100
    },
    bgImage:{
        width:Sizing.x90,
        height:Sizing.x85,
        borderRadius:Outlines.borderRadius.base
    },
    Container:{
        flexDirection:'row',
        borderBottomWidth:Outlines.borderWidth.base,
        borderBottomColor:Colors.neutral.s100,
    }
});