import React from 'react'
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { Sizing, Outlines, Colors, Typography, Theme } from "../../styles/index";

import TrendingTopicsButton from './TrendingTopicsButton';

const buttonData = [
    {
        id:1,
        text:'IPL 20',
        color:"#FF9900"
    },
    {
        id:2,
        text:'Injured',
        color:"#F56731"
    },
    {
        id:3,
        text:'Transferred',
        color:"#5B9863"
    }
]


function TrendingTopicsRender() {
    return (
        <View style={styles.Container}>
            <View style={styles.Layout__top}>
                <Text style={styles.TopicText}>Trending Topics</Text>
            </View>
            <View style={styles.Layout__bottom}>
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                    {buttonData.map((item) => (
                    <TrendingTopicsButton topic={item.text} color={item.color} 
                    key={item.id} />
                    ))}
                </ScrollView>
            </View>
        </View>
    )
}

export default TrendingTopicsRender;

const styles = StyleSheet.create({
    // layout bottom
    Layout__bottom:{
        ...Sizing.padding.a1,
        flexDirection:"row"
    },

    // layout top
    TopicText:{
        ...Typography.fontSize.x20,
        ...Typography.fontWeight.semibold,
        color:Theme.colors.lightGrey
    },
    Layout__top:{
        paddingBottom:Sizing.x10
    },

    // container 
    Container:{
        ...Sizing.padding.a3,
        ...Sizing.percentage.w95,
        backgroundColor: Colors.neutral.white,
        borderRadius: Outlines.borderRadius.large,
    }
});