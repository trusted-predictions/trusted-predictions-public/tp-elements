import React from 'react'
import { View, Text, StyleSheet } from 'react-native';
import  LatestNewsReel  from './LatestNewsReel';
import { Sizing, Outlines, Colors, Typography, Theme } from "../../styles/index";

const buttonData = [
    {
        id:1,
        date:'Date of news',
        topic:'Topic Name',
        news:'Sapient in explicabo nihil eveniet quis dolores quas.',
        source:'Source name',
        imgSrc: require('../../assets/sports.jpg'),
    },
    {
        id:2,
        date:'Date of news',
        topic:'Topic Name',
        news:'Sapient in explicabo nihil eveniet quis dolores quas.',
        source:'Source name',
        imgSrc: require('../../assets/sports2.jpg'),
    },
    {
        id:3,
        date:'Date of news',
        topic:'Topic Name',
        news:'Sapient in explicabo nihil eveniet quis dolores quas.',
        source:'Source name',
        imgSrc: require('../../assets/sports3.jpg'),
    },
]


function LatestNewsRender() {
    return (
       <View style={styles.Container}>
           <Text style={styles.newsText}>Latest News</Text>
            {buttonData.map((item) => (
                <LatestNewsReel  key={item.id} date={item.date} topic={item.topic} news={item.news} source={item.source} imageSource={item.imgSrc} />
                ))}
            
       </View>
    )
}

export default LatestNewsRender

const styles = StyleSheet.create({
    newsText:{
        ...Typography.fontSize.x20,
        ...Typography.fontWeight.semibold,
        color:Theme.colors.lightGrey
    },
    Container:{
        ...Sizing.padding.a3,
        ...Sizing.percentage.w95,
        backgroundColor: Colors.neutral.white,
        borderRadius: Outlines.borderRadius.large,
    }
});