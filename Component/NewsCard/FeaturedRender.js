import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native';
import { Sizing, Outlines, Colors, Typography, Theme } from "../../styles/index";
import TrendingTopicsButton from './TrendingTopicsButton';
import { Icon } from 'react-native-elements'

const buttonData = [
    {
        id:1,
        text:'Injured',
        color:"#F56731"
    },
]


function FeaturedRender() {
    return (
        <View style={styles.Container}>
            <View style={styles.Layout__top}>
                <Text style={styles.featuredText}>Featured</Text>
                {/* dynamic button rendering */}
                {buttonData.map((item) => (
                <TrendingTopicsButton topic={item.text} color={item.color} 
                key={item.id} />
                ))}
                <Icon  onPress={()=>{}} name="dots-three-vertical" type="entypo"  size={20} color={Theme.colors.veryLightGrey}></Icon>
            </View>
            <View style={styles.Layout__bottom}>
                <Image source={require('../../assets/sports.jpg')} style={styles.bgImage}></Image> 
                <View style={styles.textWrap}>
                    <Text style={styles.headlineText}>Headline of a news which is featured on the top of this page</Text>
                    <View style={styles.sourceWrap}>
                        <View style={styles.sourceBorder}>
                            <Text style={styles.sourceText}>Source name</Text>
                        </View>
                        <View>
                            <Text style={styles.dateText}>Date of news</Text>
                        </View>
                    </View>
                </View>  
            </View>
        </View>
    )
}

export default FeaturedRender

const styles = StyleSheet.create({
    // layout Bottom
    sourceBorder:{
        borderRightColor:Colors.success.s400,
        borderRightWidth: Outlines.borderWidth.base,
        
    },
    sourceText:{
        color:Colors.success.s400,
        paddingRight:Sizing.layout.x10
    },
    dateText:{
        color:Colors.success.s400,
        paddingLeft:Sizing.layout.x10
    },
    sourceWrap:{
        flexDirection:'row',
        alignItems:'center',
        marginTop:Sizing.layout.x10
    },
    textWrap:{
        ...Sizing.padding.a2
    },
    headlineText:{
        color:Colors.neutral.white,
        ...Typography.fontSize.x30
    },
    bgImage:{
        width:Sizing.layout.x350,
        height:Sizing.layout.x350,
        borderRadius:Outlines.borderRadius.large,
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    Layout__bottom:{
        width:Sizing.layout.x350,
        height:Sizing.layout.x350,
        ...Sizing.margin.a2,
        alignSelf:'center',
    },
    // layout top
    featuredText:{
        flex:1,
        ...Typography.fontSize.x20,
        ...Typography.fontWeight.semibold,
        color:Theme.colors.lightGrey
    },
    Layout__top:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'
    },
    // container
    Container:{
        ...Sizing.padding.a3,
        ...Sizing.percentage.w95,
        backgroundColor: Colors.neutral.white,
        borderRadius: Outlines.borderRadius.large,
    }
});