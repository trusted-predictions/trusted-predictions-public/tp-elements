import React from 'react'
import {  StyleSheet, Text, View } from 'react-native';
import { Sizing, Colors, Buttons } from '../../styles/index';

function TrendingTopicsButton({ topic, color, override }) {
    return (
        
        <View style={styles.Container}>
            <View style={styles.Custom__Button} backgroundColor={color}>
                <Text style={styles.TopicText}>{topic}</Text>
            </View>
        </View>
    )
}

export default TrendingTopicsButton;

const styles = StyleSheet.create({
    Custom__Button:{
        ...Buttons.bar.secondary,
        paddingTop: Sizing.layout.x8,
        paddingBottom: Sizing.layout.x8,
        paddingLeft: Sizing.layout.x40,
        paddingRight: Sizing.layout.x40,
        // marginRight: Sizing.layout.x20
    },
    TopicText:{
        color:Colors.neutral.white
    },
    Container:{
        flexDirection:"row"
    }
});