import React from 'react'
import { View, Text, StyleSheet } from 'react-native';
import { Sizing, Outlines, Colors, Typography, Theme } from "../../styles/index";
import VideoRewards from './VideoRewards';
function WatchVideoRender() {
    return (
        <View style={styles.Container}>
            <View style={styles.Layout__top}>
                <Text style={styles.videoText}>Watch Video</Text>
            </View>
            <View style={styles.Layout__bottom}> 
                <Text style={styles.rewardsText}>Watch videos to recieve rewards</Text>
                <VideoRewards />
            </View>
        </View>
    )
}

export default WatchVideoRender

const styles = StyleSheet.create({
    Layout__bottom:{
        ...Sizing.padding.a2,
    },
    rewardsText:{
        ...Typography.fontSize.x20,
        color: Colors.neutral.s500,
    },
    Layout__top:{
        ...Sizing.padding.a2,
        borderBottomWidth: Outlines.borderWidth.base,
        borderBottomColor: Theme.colors.lightestGrey,
    },
    videoText:{
        ...Typography.fontSize.x30,
        ...Typography.fontWeight.bold
    },
    Container:{
        ...Sizing.padding.a3,
        ...Sizing.percentage.w95,
        backgroundColor: Colors.neutral.white,
        borderRadius: Outlines.borderRadius.large,
    }
});