import React from 'react'
import { View, Text, StyleSheet } from 'react-native';
import { Sizing, Outlines, Colors, Typography, Theme } from "../../styles/index";
import { Icon } from 'react-native-elements';



function AvailBalanceRender() {
    return (
        <View style={styles.Container}>
            <Text style={styles.balanceText}>Available Balance</Text>
            <View style={styles.iconTextWrap}>
                <Icon name="coins" type="font-awesome-5"  size={25} color='#FFC400'></Icon>
                <Text style={styles.coinText}>350</Text>
            </View>
        </View>
    )
}

export default AvailBalanceRender

const styles = StyleSheet.create({
    iconTextWrap:{
        flexDirection:'row',
        alignItems:'center',
    },
    coinText:{
        ...Typography.fontSize.x40,
        ...Typography.fontWeight.bold,
        color:Colors.success.s400,
        marginLeft:Sizing.layout.x10
    },
    balanceText:{
        ...Typography.fontSize.x30,
        ...Typography.fontWeight.semibold,
        color:Theme.colors.veryLightGrey,
        flex:1
    },
    Container:{
        ...Sizing.padding.a3,
        paddingTop: Sizing.layout.x30,
        paddingBottom: Sizing.layout.x30,
        ...Sizing.percentage.w95,
        backgroundColor: Colors.neutral.white,
        borderRadius: Outlines.borderRadius.large,
        flexDirection:'row',
        alignItems:'center'
    }
});