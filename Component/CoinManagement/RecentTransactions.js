import React from 'react'
import { View, StyleSheet, Text } from 'react-native';
import { Sizing, Outlines, Colors, Typography, Theme } from "../../styles/index";
import RecentTransactionReel from './RecentTransactionReel';

const data =[
    {
        id:1,
        transactionDate:'[Date of transaction]',
        time:'9:30 AM',
        coin1:'-20 Coins',
        coin2:'+20 Coins',
        balanceCoin:'350 Coins'
    },
    {
        id:2,
        transactionDate:'[Date of transaction]',
        time:'9:30 AM',
        coin1:'-20 Coins',
        coin2:'+20 Coins',
        balanceCoin:'350 Coins'
    },
    {
        id:3,
        transactionDate:'[Date of transaction]',
        time:'9:30 AM',
        coin1:'-20 Coins',
        coin2:'+20 Coins',
        balanceCoin:'350 Coins'
    }
]
function RecentTransactions() {
    return (
        <View style={styles.Container}> 
            <View style={styles.Layout__top}>
                <Text style={styles.transactionText}>Recent Transactions</Text>
            </View>
            <View>
                {data.map((item) => (
                    <RecentTransactionReel key={item.id} transactionDate={item.transactionDate}  time={item.time} coin1={item.coin1} coin2={item.coin2} balanceCoin={item.balanceCoin}  />
                    ))}
            </View>
        </View>
    )
}

export default RecentTransactions

const styles = StyleSheet.create({
    Container: {
      ...Sizing.padding.a3,
      ...Sizing.percentage.w95,
      backgroundColor: Colors.neutral.white,
      borderRadius: Outlines.borderRadius.large,
    },
    Layout__top: {
        ...Sizing.padding.a2,
        borderBottomWidth: Outlines.borderWidth.base,
        borderBottomColor: Theme.colors.lightestGrey,
    },
    transactionText: {
        ...Typography.fontSize.x30,
        ...Typography.fontWeight.bold,
    },
  });
  