import React from 'react'
import { View, StyleSheet, Text } from 'react-native';
import { Sizing, Outlines, Colors, Typography, Theme } from "../../styles/index";

function RecentTransactionReel({transactionDate, time, coin1, coin2, balanceCoin}) {
    return (
        <View style={styles.Container}>
            <Text style={styles.transactionText}>{transactionDate}</Text>
            <View style={styles.Layout__bottom}>
                <View style={styles.coinBar}>
                    <Text style={styles.usageText}>[Where coins were used]</Text>
                    <Text style={styles.dateBalance}>{time}</Text>
                </View>
                <View >
                    <Text style={styles.balanceCoin}>{coin1}</Text>
                    <Text style={styles.dateBalance}>Closing balance: {balanceCoin}</Text>
                </View>
            </View>
            <View style={styles.Layout__bottom}>
                <View style={styles.coinBar}>
                    <Text style={styles.usageText}>[Where coins were used]</Text>
                    <Text style={styles.dateBalance}>{time}</Text>
                </View>
                <View >
                    <Text style={styles.balanceCoin2}>{coin2}</Text>
                    <Text style={styles.dateBalance}>Closing balance: {balanceCoin}</Text>
                </View>
            </View>
        </View>
    )
}

export default RecentTransactionReel

const styles = StyleSheet.create({
    dateBalance:{
        ...Typography.fontSize.x10,
        color:Theme.colors.veryLightGrey,
        paddingBottom:Sizing.x20
    },
    usageText:{
        ...Typography.fontWeight.bold,
        paddingTop:Sizing.x10,
        paddingBottom:Sizing.x8
    },
    balanceCoin2:{
        alignSelf:'flex-end',
        color:Colors.success.s400,
        ...Typography.fontWeight.bold,
        paddingTop:Sizing.x10,
        paddingBottom:Sizing.x8
    },
    balanceCoin:{
        alignSelf:'flex-end',
        color:Colors.danger.s400,
        ...Typography.fontWeight.bold,
        paddingTop:Sizing.x10,
        paddingBottom:Sizing.x8
    },
    coinBar:{
        flex:1,
    },
    Layout__bottom:{
        flexDirection:'row',
        borderBottomWidth: Outlines.borderWidth.base,
        borderBottomColor: Theme.colors.lightestGrey,
    },
    Container: {
        ...Sizing.padding.a2,
    },
    transactionText: {
        color:Colors.success.s400,
        ...Typography.fontWeight.semibold,
    },
  });
  