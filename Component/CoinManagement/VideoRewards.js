import React from 'react'
import { View, Text, StyleSheet } from 'react-native';
import { Sizing, Outlines, Colors, Typography, Theme } from "../../styles/index";
import TrendingTopicsButton from '../NewsCard/TrendingTopicsButton';

const data = {
    topic:'Watch video',
    color: Colors.primary.brand
}

function VideoRewards() {
    return (
        <View style={styles.Container}>
            <View>
                <Text style={styles.rewardsText}>Free Rewards</Text>
                <Text style={styles.coinsText}>100-500 Coins</Text>
            </View>
            <View style={styles.Layout__bottom}>
                <TrendingTopicsButton  topic={data.topic} color={data.color}/>
            </View>
            
        </View>
    )
}

export default VideoRewards

const styles = StyleSheet.create({
    Layout__bottom:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end'
    },
    coinsText:{
        color:Colors.success.s400,
        paddingTop:Sizing.x10
    },
    rewardsText:{
        color:Theme.colors.lightDark,
        ...Typography.fontWeight.semibold,
        ...Typography.fontSize.x20
    },
    
    Container:{
        marginTop:Sizing.x20,
        ...Sizing.padding.a3,
        ...Sizing.percentage.w90,
        backgroundColor: Colors.neutral.white,
        borderRadius: Outlines.borderRadius.large,
        backgroundColor: Colors.neutral.white,
        borderRadius: Outlines.borderRadius.large,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4.84,
        elevation: 5,
        alignSelf:'center',
        flexDirection:'row',
        alignItems:'center'
    }
});