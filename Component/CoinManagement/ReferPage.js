import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
} from "react-native";
import { TouchableRipple } from "react-native-paper";
import { Typography, Sizing, Outlines, Colors, Buttons, Theme } from "../../styles/index";

export default function ReferPage() {
  return (
    <View style={styles.container}>
      <View style={styles.Layout__top}>
        <Text style={styles.referText}>Refer and Earn</Text>
      </View>
      <View style={styles.Layout__Bottom}>
        <Text style={styles.inviteText}>
          Invite your friend to our platform and you'll both recieve 100 coins
          when he will join the platform
        </Text>
        <View>
          <ImageBackground
            style={styles.bgImage}
            resizeMode="contain"
            source={require("../../assets/images/refer.png")}
          >
            <View style={styles.wrap__field}>
              <View style={styles.inputField}>
                <Text style={styles.referLinkText}>bit.ly/akjfhlw3i/jah</Text>
              </View>
              <TouchableRipple onPress={() => {}} style={styles.customButton}>
                <Text style={styles.ButtonText}>Share Link</Text>
              </TouchableRipple>
            </View>
          </ImageBackground>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    ...Sizing.padding.a3,
    ...Sizing.percentage.w95,
    backgroundColor: Colors.neutral.white,
    borderRadius: Outlines.borderRadius.large,
  },
  // Background image
  bgImage: {
    height: 250,
    width: 250,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 8,
    alignSelf: "center",
  },
  // Button inside text input field
  ButtonText: {
    color: Colors.neutral.white,
    ...Typography.fontSize.x20,
    ...Typography.fontWeight.bold,
  },
  customButton: {
    ...Buttons.bar.secondary,
    backgroundColor: Colors.primary.s200,
    borderRadius: Outlines.borderRadius.large,
    paddingHorizontal: Sizing.layout.x20,
  },
  inputField: {
    ...Sizing.percentage.w50,
    paddingLeft: Sizing.x20,
    borderRadius: Outlines.borderRadius.large,
    justifyContent: "center",
    flexGrow: 1,
  },
  wrap__field: {
    ...Sizing.percentage.w75,
    flexDirection: "row",
    alignSelf: "center",
    backgroundColor: Colors.neutral.white,
    borderRadius: Outlines.borderRadius.large,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4.84,
    elevation: 5,
  },

  // Layout bottom
  Layout__Bottom: {
    ...Sizing.padding.a2,
  },
  inviteText: {
    ...Typography.fontSize.x20,
    color: Colors.neutral.s500,
  },
  // Layout top
  Layout__top: {
    ...Sizing.padding.a2,
    borderBottomWidth: Outlines.borderWidth.base,
    borderBottomColor: Theme.colors.lightestGrey,
  },
  referText: {
    ...Typography.fontSize.x30,
    ...Typography.fontWeight.bold,
  },
  referLinkText: {
    color: Colors.success.s400,
    ...Typography.fontSize.x20,
    ...Typography.fontWeight.bold,
  },
});
