import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Sizing, Typography } from "../styles";
import { colors } from "../styles/theme";
import FallOfWicketsStatsRow from "./FallOfWicketsStatsRow";

const fallOfWickets = [
  { wicket: "1", over: "8.5", runs: "60", player: "K L Rahul" },
  { wicket: "2", over: "20", runs: "124", player: "Sinkgar Dhawan" },
  { wicket: "3", over: "23.5", runs: "146", player: "Virat Kohli" },
  { wicket: "4", over: "38.2", runs: "247", player: "Shreyash Iyer" },
];

const FallOfWicketsTable = () => {
  return (
    <>
      <View style={styles.row}>
        <Text style={[styles.headingText, { width: 50, textAlign: "left" }]}>
          wickets
        </Text>
        <Text style={[{ width: 40 }, styles.headingText]}>Over</Text>
        <Text style={[{ width: 40 }, styles.headingText]}>runs</Text>
        <Text style={[styles.headingText, { width: 95, textAlign: "right" }]}>
          player
        </Text>
      </View>
      {fallOfWickets.map((item) => (
        <FallOfWicketsStatsRow data={item} key={item.wicket} />
      ))}
    </>
  );
};

export default FallOfWicketsTable;

const styles = StyleSheet.create({
  headingText: {
    color: colors.veryLightGrey,
    ...Typography.fontSize.x15,
    textAlign: "center",
    textTransform: "capitalize",
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: Sizing.layout.x4,
  },
});
