import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Card, Divider } from "react-native-elements";
import { Typography } from "../../styles";
import { cardStyles } from "../../styles/card";
import { colors } from "../../styles/theme";
import BatsmanStatsTable from "../BatsmanStatsTable";
import BowlerStatsTable from "../BowlerStatsTable";
import FallOfWicketsTable from "../FallOfWicketsTable";
import SelectInnings from "../SelectInnings";

const playerBattingData = [
  {
    name: "Rohit Sharma",
    run: "88(70)",
    "4s": 4,
    "6s": 4,
    SR: "125.71",
    id: "1",
  },
  {
    name: "K L Rahul",
    run: "48(40)",
    "4s": 4,
    "6s": 1,
    SR: "120",
    id: "3",
    out: "c Smith b Cummins",
  },
  {
    name: "Virat Kohli",
    run: "63(54)",
    "4s": 3,
    "6s": 0,
    SR: "112",
    id: "4",
    out: "b Zampa",
  },
  {
    name: "Shreyash Iyer",
    run: "6(6)",
    "4s": 0,
    "6s": 0,
    SR: "100",
    id: "5",
    out: "c Starc b Cummins",
  },
  {
    name: "Manish Pandey",
    run: "48(40)",
    "4s": 4,
    "6s": 1,
    SR: "120",
    id: "6",
  },
];

const playerBowlingData = [
  {
    name: "Pat Cummins",
    over: "8.5",
    m: 1,
    r: 40,
    w: 1,
    eco: "4.6",
    id: "1",
  },
  {
    name: "J. Hazelwood",
    over: "8.5",
    m: 1,
    r: 40,
    w: 1,
    eco: "4.6",
    id: "2",
  },
  {
    name: "M. Starc",
    over: "8.5",
    m: 1,
    r: 40,
    w: 1,
    eco: "4.6",
    id: "3",
  },
  {
    name: "Kane Richerdson",
    over: "8.5",
    m: 1,
    r: 40,
    w: 1,
    eco: "4.6",
    id: "4",
  },
  {
    name: "Adam Zampa",
    over: "8.5",
    m: 1,
    r: 40,
    w: 1,
    eco: "4.6",
    id: "5",
  },
];

const extras = {
  "no ball": 2,
  wide: 17,
  bye: 6,
  "l-bye": 17,
  penalty: 2,
};

const Scorecard = () => {
  const [currentIndex, setCurrentIndex] = useState(0);

  return (
    <>
      {/* Select Innings */}
      <SelectInnings
        currentIndex={currentIndex}
        setCurrentIndex={setCurrentIndex}
      />
      {currentIndex === 0 ? (
        <>
          {/* Batting */}
          <Card containerStyle={cardStyles.cardContainer}>
            <View style={cardStyles.cardHeaderContainer}>
              <Text style={cardStyles.cardTitle}>batting</Text>
            </View>
            <BatsmanStatsTable
              playerData={playerBattingData}
              showWicketTakers
            />
          </Card>
          {/* bowling */}
          <Card containerStyle={cardStyles.cardContainer}>
            <View style={cardStyles.cardHeaderContainer}>
              <Text style={cardStyles.cardTitle}>bowling</Text>
            </View>
            <BowlerStatsTable playerData={playerBowlingData} />
          </Card>
          {/* Fall of wickets */}
          <Card containerStyle={cardStyles.cardContainer}>
            <View style={cardStyles.cardHeaderContainer}>
              <Text style={cardStyles.cardTitle}>Fall Of Wickets</Text>
            </View>
            <FallOfWicketsTable />
          </Card>
          <Card
            containerStyle={[cardStyles.cardContainer, { marginBottom: 16 }]}
          >
            <View style={{ position: "relative" }}>
              <View style={styles.dot} />
              <Text style={styles.headingText}>Extras</Text>
            </View>

            <View style={styles.rowItem}>
              {Object.keys(extras).map((item) => (
                <Text style={styles.primaryText}>
                  {item}
                  <Text style={styles.greyText}>{` ${extras[item]}`}</Text>
                </Text>
              ))}
            </View>
            <View style={{ position: "relative" }}>
              <View style={styles.dot} />
              <Text style={styles.headingText}>Umpires</Text>
            </View>

            <View style={styles.rowItem}>
              <Text style={styles.primaryText}>{"Field umpires: "}</Text>
              <Text style={styles.greyText}>Enola D'Amore, Rocky Walter</Text>
            </View>
            <View style={styles.rowItem}>
              <Text style={styles.primaryText}>{"Third umpire:  "}</Text>
              <Text style={styles.greyText}>Annie Langosh</Text>
            </View>
            <View style={{ position: "relative" }}>
              <View style={styles.dot} />
              <Text style={styles.headingText}>Ground</Text>
            </View>
            <Text style={[styles.greyText, { marginBottom: 12 }]}>
              Name of the ground, place
            </Text>

            <View style={{ position: "relative" }}>
              <View style={styles.dot} />
              <Text style={styles.headingText}>Other Details</Text>
            </View>
            <Text style={[styles.greyText]}>
              All other details about the match
            </Text>
          </Card>
        </>
      ) : (
        <>
          <Card containerStyle={[cardStyles.cardContainer, styles.emptyCard]}>
            <Text style={styles.emptyText}>Second innings not started yet</Text>
          </Card>
        </>
      )}
    </>
  );
};

export default Scorecard;

const styles = StyleSheet.create({
  emptyText: {
    ...Typography.fontSize.x25,
    color: colors.lightGrey,
  },
  primaryText: {
    marginRight: 8,
    color: colors.success,
    ...Typography.fontSize.x15,
    textTransform: "capitalize",
  },
  headingText: {
    color: colors.veryLightGrey,
    ...Typography.fontSize.x20,
    marginBottom: 2,
    textTransform: "capitalize",
  },
  emptyCard: { height: 200, justifyContent: "center", alignItems: "center" },
  rowItem: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginBottom: 12,
    alignItems: "center",
  },
  greyText: { color: colors.grey, fontSize: 16 },
  dot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    backgroundColor: colors.primary,
    position: "absolute",
    left: -11,
    top: 5,
  },
});
