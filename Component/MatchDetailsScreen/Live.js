import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Card, Divider } from "react-native-elements";
import { Sizing, Typography } from "../../styles";
import { cardStyles } from "../../styles/card";
import { colors } from "../../styles/theme";
import BatsmanStatsTable from "../BatsmanStatsTable";
import BowlerStatsTable from "../BowlerStatsTable";
import WhoWillWin from "../Predections/WhoWillWin";
import ProjectedScore from "./ProjectedScore";

const playerBattingData = [
  {
    name: "Rohit Sharma",
    run: "88(70)",
    "4s": 4,
    "6s": 4,
    SR: "125.71",
    id: "1",
  },
  {
    name: "Manish Pandey",
    run: "48(40)",
    "4s": 4,
    "6s": 1,
    SR: "120",
    id: "2",
  },
];

const playerBowlingData = [
  {
    name: "Adam Zampa",
    over: "8.5",
    m: 1,
    r: 40,
    w: 1,
    eco: "4.6",
    id: "1",
  },
];

const Live = () => {
  return (
    <>
      <WhoWillWin />
      <Card containerStyle={cardStyles.cardContainer}>
        <View style={{ marginBottom: Sizing.layout.x8 }}>
          <Text style={cardStyles.cardTitle}>Scorecard</Text>
        </View>
        <BatsmanStatsTable playerData={playerBattingData} />
        <Divider style={{ marginVertical: 4 }} />
        <BowlerStatsTable playerData={playerBowlingData} />
      </Card>
      <Card containerStyle={{ borderRadius: Sizing.layout.x16 }}>
        <Text style={styles.redText}>Current Run Rate: 7.25 RPO</Text>
      </Card>
      <ProjectedScore />
    </>
  );
};

export default Live;

const styles = StyleSheet.create({
  redText: {
    ...Typography.fontSize.x20,
    color: colors.danger,
  },
});
