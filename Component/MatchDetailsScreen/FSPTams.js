import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Divider, Card } from "react-native-elements";

import { cardStyles } from "../../styles/card";

const FSPTams = () => {
  return (
    <>
      <Card style={cardStyles.cardContainer}>
        <View style={cardStyles.cardHeaderContainer}>
          <Text style={cardStyles.cardTitle}>Recommended fantasy teams</Text>
        </View>
        <Divider />
      </Card>
    </>
  );
};

export default FSPTams;

const styles = StyleSheet.create({});
