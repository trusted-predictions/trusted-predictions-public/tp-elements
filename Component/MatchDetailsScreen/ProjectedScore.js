import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Card } from "react-native-elements";
import { ProgressBar } from "react-native-paper";
import { Sizing, Typography } from "../../styles";
import { cardStyles } from "../../styles/card";
import { colors } from "../../styles/theme";
import { Bar } from "react-native-progress";

const data = [
  { name: "india", id: 1, score: 320, current: 240 },
  { name: "australia", id: 2, score: 310, current: 0 },
];

const ScoreProgress = ({ data }) => {
  const { name, score, current } = data;
  return (
    <View style={[styles.row, { marginVertical: Sizing.layout.x5 }]}>
      <View
        style={{
          width: 25,
          height: 25,
          backgroundColor: colors.veryLightGrey,
          borderRadius: 8,
        }}
      />
      <View style={{ flexGrow: 1, marginLeft: 8 }}>
        <View style={[styles.row, { justifyContent: "space-between" }]}>
          <Text
            style={{ color: colors.veryLightGrey, textTransform: "capitalize" }}
          >
            {name}
          </Text>
          <Text style={{ color: colors.success, textTransform: "capitalize" }}>
            Projected Score: {score}
          </Text>
        </View>
        <Bar
          progress={current ? current / score : 0}
          width={null}
          height={12}
          style={{ marginTop: 4 }}
          borderRadius={8}
          color={colors.primary}
          borderColor={colors.primary}
        />
      </View>
    </View>
  );
};

const ProjectedScore = () => {
  return (
    <Card
      containerStyle={[
        cardStyles.cardContainer,
        { marginBottom: Sizing.layout.x20 },
      ]}
    >
      <View style={[styles.row, { marginBottom: Sizing.layout.x8 }]}>
        <Text style={cardStyles.cardTitle}>Projected Scores</Text>
        <Text style={styles.secondaryText}>1st innings</Text>
      </View>
      {data.map((item) => (
        <ScoreProgress data={item} key={item.id} />
      ))}
    </Card>
  );
};

export default ProjectedScore;

const styles = StyleSheet.create({
  secondaryText: {
    ...Typography.fontSize.x20,
    color: colors.veryLightGrey,
    marginLeft: "auto",
  },
  row: { flexDirection: "row", alignItems: "center" },
});
