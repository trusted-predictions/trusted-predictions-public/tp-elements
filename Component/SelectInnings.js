import React, { useRef } from "react";
import { StyleSheet, Text, View, Animated } from "react-native";
import PropTypes from "prop-types";
import { colors } from "../styles/theme";
import { TouchableRipple } from "react-native-paper";
import { Sizing, Typography } from "../styles";
import { Dimensions } from "react-native";

const { width } = Dimensions.get("window");

const INDICATOR_WIDTH = (width - 28 - 24) / 2;

const SelectInnings = ({ currentIndex, setCurrentIndex }) => {
  const translateX = useRef(new Animated.Value(0)).current;

  const getActiveColor = (index) => {
    if (currentIndex === index) {
      return colors.primary;
    }
    return colors.lightGrey;
  };

  const goTo2ndInnings = () => {
    if (currentIndex === 1) return;
    Animated.timing(translateX, {
      toValue: INDICATOR_WIDTH,
      duration: 160,
      useNativeDriver: true,
    }).start(() => setCurrentIndex(1));
  };

  const goTo1stIndInnings = () => {
    if (currentIndex === 0) return;
    Animated.timing(translateX, {
      toValue: 0,
      duration: 160,
      useNativeDriver: true,
    }).start(() => setCurrentIndex(0));
  };

  return (
    <View style={styles.container}>
      <TouchableRipple
        borderless
        rippleColor={colors.primary}
        disabled={currentIndex === 0}
        onPress={goTo1stIndInnings}
        style={styles.touchable}
      >
        <Text style={{ ...Typography.fontSize.x20, color: getActiveColor(0) }}>
          1s Innings
        </Text>
      </TouchableRipple>
      <TouchableRipple
        borderless
        disabled={currentIndex === 1}
        onPress={goTo2ndInnings}
        rippleColor={colors.primary}
        style={styles.touchable}
      >
        <Text style={{ ...Typography.fontSize.x20, color: getActiveColor(1) }}>
          2nd Innings
        </Text>
      </TouchableRipple>
      <Animated.View
        style={[styles.indicator, { transform: [{ translateX }] }]}
      />
    </View>
  );
};

SelectInnings.propTypes = {
  currentIndex: PropTypes.number.isRequired,
  setCurrentIndex: PropTypes.func.isRequired,
};

export default SelectInnings;

const styles = StyleSheet.create({
  touchable: {
    flex: 1,
    alignItems: "center",
    height: 45,
    justifyContent: "center",
    borderRadius: Sizing.layout.x5,
  },
  indicator: {
    width: INDICATOR_WIDTH,
    height: Sizing.layout.x4,
    backgroundColor: colors.primary,
    position: "absolute",
    left: Sizing.layout.x12,
    bottom: 0,
    borderRadius: Sizing.layout.x5,
  },
  container: {
    flexDirection: "row",
    backgroundColor: colors.white,
    marginHorizontal: 14,
    marginTop: Sizing.layout.x8,
    position: "relative",
    height: 45,
    borderRadius: Sizing.layout.x12,
    alignItems: "center",
  },
});
