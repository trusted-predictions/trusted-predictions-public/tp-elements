import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { colors } from "../../styles/theme";
import { fontSize } from "../../styles/typography";
import * as Sizing from "../../styles/sizing";

const RED_DOT_HEIGHT = Sizing.layout.x9;

const LiveIcon = ({ style = {} }) => {
  return (
    <View style={[styles.container, style]}>
      <View style={styles.dot} />
      <Text style={{ color: colors.white, ...fontSize.x10 }}>LIVE</Text>
    </View>
  );
};

export default LiveIcon;

const styles = StyleSheet.create({
  dot: {
    width: RED_DOT_HEIGHT,
    height: RED_DOT_HEIGHT,
    backgroundColor: colors.deepPink,
    borderRadius: RED_DOT_HEIGHT / 2,
    marginRight: Sizing.layout.x8 / 2,
  },
  container: {
    backgroundColor: colors.lightDark,
    paddingVertical: Sizing.layout.x2,
    paddingHorizontal: Sizing.layout.x5,
    marginRight: Sizing.layout.x5,
    borderRadius: Sizing.layout.x5,
    flexDirection: "row",
    alignItems: "center",
  },
});
