import React from "react";
import { StyleSheet, Text, View } from "react-native";
import PropTypes from "prop-types";
import { Sizing, Typography } from "../styles";
import { colors } from "../styles/theme";
import BowlerBowlingStatsRow from "./BowlerBowlingStatsRow";
import { bowlingTableWidth } from "../styles/tableWidth";

const BowlerStatsTable = ({ playerData }) => {
  return (
    <>
      <View style={styles.row}>
        <Text
          style={[
            styles.headingText,
            { width: bowlingTableWidth.nameWidth, textAlign: "left" },
          ]}
        >
          player
        </Text>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Text style={[{ width: bowlingTableWidth.over }, styles.headingText]}>
            over
          </Text>
          <Text
            style={[{ width: bowlingTableWidth.small }, styles.headingText]}
          >
            m
          </Text>
          <Text
            style={[{ width: bowlingTableWidth.small }, styles.headingText]}
          >
            r
          </Text>
          <Text style={[{ width: bowlingTableWidth.mid }, styles.headingText]}>
            eco
          </Text>
          <Text
            style={[styles.headingText, { width: bowlingTableWidth.small }]}
          >
            {" "}
            W
          </Text>
        </View>
      </View>
      {playerData.map((item) => (
        <BowlerBowlingStatsRow data={item} key={item.id} />
      ))}
    </>
  );
};

BowlerStatsTable.propTypes = {
  playerData: PropTypes.array.isRequired,
};

export default BowlerStatsTable;

const styles = StyleSheet.create({
  headingText: {
    color: colors.veryLightGrey,
    ...Typography.fontSize.x15,
    textAlign: "center",
    textTransform: "capitalize",
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: Sizing.layout.x4,
  },
});
