import React from "react";
import { Image } from "react-native";
import { TouchableOpacity } from "react-native";
import { StyleSheet, Text, View } from "react-native";
import { Divider, Icon } from "react-native-elements";
import { Sizing, Typography } from "../styles";
import { percentage } from "../styles/sizing";
import { colors } from "../styles/theme";

const optionsOne = [
  { title: "Home", onPress: () => {} },
  { title: "indian premier league", onPress: () => {} },
  { title: "coins management", onPress: () => {} },
  { title: "marketplace", onPress: () => {} },
  { title: "settings", onPress: () => {} },
];

const optionsTwo = [
  { title: "Feedback", onPress: () => {} },
  { title: "Terms & Privacy", onPress: () => {} },
];

const Drawer = () => {
  const handleLogout = () => {};

  return (
    <View
      style={{
        ...percentage.w80,
        backgroundColor: colors.white,
        padding: Sizing.layout.x1616,
        position: "relative",
        flex: 1,
      }}
    >
      <View style={{ flexGrow: 1 }}>
        <Image
          source={require("../assets/images/user.png")}
          style={{ width: 100, height: 100 }}
          resizeMode="contain"
        />
        <Text style={styles.boldText}>Madyson52</Text>
        <TouchableOpacity activeOpacity={0.8} style={styles.editProfile}>
          <Text style={{ color: colors.veryLightGrey }}>Edit Profile</Text>
        </TouchableOpacity>
        <View style={{ marginTop: Sizing.layout.x30 }}>
          {optionsOne.map((option) => (
            <TouchableOpacity
              onPress={option.onPress}
              activeOpacity={0.6}
              style={styles.item}
            >
              <Text style={styles.secondaryText}>{option.title}</Text>
            </TouchableOpacity>
          ))}
        </View>
        <Divider style={{ marginVertical: Sizing.layout.x8 }} />
        {optionsTwo.map((option) => (
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={option.onPress}
            style={styles.item}
          >
            <Text style={styles.secondaryText}>{option.title}</Text>
          </TouchableOpacity>
        ))}
        <TouchableOpacity
          activeOpacity={0.6}
          onPress={handleLogout}
          style={styles.item}
        >
          <Text style={[styles.secondaryText, { color: colors.veryLightGrey }]}>
            Logout
          </Text>
        </TouchableOpacity>
        <Divider style={{ marginTop: Sizing.layout.x8 }} />
      </View>
      <View style={{ position: "absolute", right: 8, top: 8 }}>
        <Icon
          onPress={() => console.log("close")}
          name="cross"
          type="entypo"
          color={colors.primary}
        />
      </View>
    </View>
  );
};

export default Drawer;

const styles = StyleSheet.create({
  boldText: {
    ...Typography.fontSize.x40,
    ...Typography.fontWeight.bold,
    marginTop: Sizing.layout.x5,
  },
  editProfile: { paddingVertical: 2 },
  secondaryText: {
    ...Typography.fontSize.x30,
    color: colors.lightGrey,
    textTransform: "capitalize",
  },
  item: {
    paddingVertical: Sizing.layout.x4,
    marginVertical: Sizing.layout.x2,
  },
});
