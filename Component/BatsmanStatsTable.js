import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Sizing, Typography } from "../styles";
import PropTypes from "prop-types";
import { colors } from "../styles/theme";
import BatsmanBattingStatsRow from "./BatsmanBattingStatsRow";
import { battingTableWidth } from "../styles/tableWidth";

const BatsmanStatsTable = ({ playerData, showWicketTakers }) => {
  return (
    <>
      <View style={styles.row}>
        <Text
          style={[
            styles.headingText,
            { width: battingTableWidth.nameWidth, textAlign: "left" },
          ]}
        >
          Player
        </Text>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Text
            style={[{ width: battingTableWidth.runsWidth }, styles.headingText]}
          >
            Runs(B)
          </Text>
          <Text
            style={[
              { width: battingTableWidth.boundariesWidth },
              styles.headingText,
            ]}
          >
            4s
          </Text>
          <Text
            style={[
              { width: battingTableWidth.boundariesWidth },
              styles.headingText,
            ]}
          >
            6s
          </Text>
          <Text
            style={[
              styles.headingText,
              { width: battingTableWidth.strikeRateWidth, textAlign: "right" },
            ]}
          >
            SR
          </Text>
        </View>
      </View>
      {playerData.map((item) => (
        <BatsmanBattingStatsRow
          data={item}
          key={item.id}
          showWicketTakers={showWicketTakers}
        />
      ))}
    </>
  );
};

BatsmanStatsTable.defaultProps = {
  showWicketTakers: false,
};

BatsmanBattingStatsRow.propTypes = {
  playerData: PropTypes.array.isRequired,
  showWicketTakers: PropTypes.bool,
};

export default BatsmanStatsTable;

const styles = StyleSheet.create({
  headingText: {
    color: colors.veryLightGrey,
    ...Typography.fontSize.x15,
    textAlign: "center",
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: Sizing.layout.x4,
  },
});
