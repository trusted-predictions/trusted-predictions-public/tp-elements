import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Sizing, Typography } from "../styles";
import { colors } from "../styles/theme";

const fallOfWickets = [
  { wicket: "1", over: "8.5", runs: "60", player: "K L Rahul" },
  { wicket: "2", over: "20", runs: "124", player: "Sinkgar Dhawan" },
  { wicket: "3", over: "23.5", runs: "146", player: "Virat Kohli" },
  { wicket: "4", over: "38.2", runs: "247", player: "Shreyash Iyer" },
];

const FallOfWicketsStatsRow = ({ data }) => {
  const { wicket, over, runs, player } = data;
  return (
    <>
      <View style={styles.row}>
        <Text style={{ width: 50, textAlign: "left" }}>{wicket}</Text>
        <Text style={[{ width: 40 }, styles.text]}>{over}</Text>
        <Text style={[{ width: 40 }, styles.text]}>{runs}</Text>
        <Text
          numberOfLines={1}
          adjustsFontSizeToFit
          style={{ width: 95, textAlign: "right" }}
        >
          {player}
        </Text>
      </View>
    </>
  );
};

export default FallOfWicketsStatsRow;

const styles = StyleSheet.create({
  text: {
    textAlign: "center",
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: Sizing.layout.x4,
  },
});
