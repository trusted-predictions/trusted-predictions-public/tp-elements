import React, { useRef, useState } from "react";
import { StyleSheet, View, Animated, ScrollView } from "react-native";
import { Button } from "react-native-elements";
import PropTypes from "prop-types";
import { percentage } from "../styles/sizing";
import { Sizing, Typography } from "../styles";
import { colors } from "../styles/theme";

const ANIMATION_DURATION = 250;

const END_OPACITY = 1;

const TwoTabs = ({ tab1name, tab1, tab2name, tab2, large }) => {
  const [selectedTabIndex, setSelectedTabIndex] = useState(0);
  const tab1Opacity = useRef(new Animated.Value(1)).current;
  const tab2Opacity = useRef(new Animated.Value(END_OPACITY)).current;

  const openTab2 = () => {
    if (selectedTabIndex === 1) return;
    setSelectedTabIndex(1);
    // Animated.timing(tab1Opacity, {
    //   toValue: END_OPACITY,
    //   useNativeDriver: true,
    //   duration: ANIMATION_DURATION,
    // }).start(() => {
    //   setSelectedTabIndex(1);
    //   Animated.timing(tab2Opacity, {
    //     toValue: 1,
    //     useNativeDriver: true,
    //     duration: ANIMATION_DURATION,
    //   }).start();
    // });
  };

  const openTab1 = () => {
    if (selectedTabIndex === 0) return;
    setSelectedTabIndex(0);
    // Animated.timing(tab2Opacity, {
    //   toValue: END_OPACITY,
    //   useNativeDriver: true,
    //   duration: ANIMATION_DURATION,
    // }).start(() => {
    //   setSelectedTabIndex(0);
    //   Animated.timing(tab1Opacity, {
    //     toValue: 1,
    //     useNativeDriver: true,
    //     duration: ANIMATION_DURATION,
    //   }).start();
    // });
  };

  const getButtonBorderColor = (index) =>
    index === selectedTabIndex ? colors.primary : colors.white;

  const getButtonTextColor = (index) =>
    index === selectedTabIndex ? colors.primary : colors.veryLightGrey;

  return (
    <ScrollView bounces={false}>
      <View style={styles.btnContainer}>
        <Button
          title={tab1name}
          type="outline"
          onPress={openTab1}
          raised={selectedTabIndex === 0}
          buttonStyle={{
            borderColor: getButtonBorderColor(0),
          }}
          titleStyle={[styles.btnTextStyles, { color: getButtonTextColor(0) }]}
          containerStyle={[
            {
              margin: Sizing.layout.x5,
            },
            large ? percentage.w45 : percentage.w35,
          ]}
        />
        <Button
          title={tab2name}
          type="outline"
          onPress={openTab2}
          raised={selectedTabIndex === 1}
          buttonStyle={{ borderColor: getButtonBorderColor(1) }}
          titleStyle={[styles.btnTextStyles, { color: getButtonTextColor(1) }]}
          containerStyle={[
            {
              margin: Sizing.layout.x5,
            },
            large ? percentage.w45 : percentage.w35,
          ]}
        />
      </View>
      {selectedTabIndex === 0 ? (
        <Animated.View
        // style={{
        //   opacity: tab1Opacity,
        // }}
        >
          {tab1()}
        </Animated.View>
      ) : (
        <Animated.View
        // style={{
        //   opacity: tab2Opacity,
        // }}
        >
          {tab2()}
        </Animated.View>
      )}
    </ScrollView>
  );
};

TwoTabs.defaultProps = {
  tab1name: "TAB 1",
  tab2name: "TAB 2",
  tab1: () => {},
  tab2: () => {},
  large: false,
};

TwoTabs.propTypes = {
  tab1name: PropTypes.string,
  tab1: PropTypes.func.isRequired,
  tab2name: PropTypes.string,
  tab2: PropTypes.func.isRequired,
  large: PropTypes.bool,
};

export default TwoTabs;

const styles = StyleSheet.create({
  btnContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: Sizing.layout.x8,
  },
  btnTextStyles: {
    ...Typography.fontSize.x20,
    ...Typography.fontWeight.bold,
  },
});
