import React from "react";
import { ScrollView } from "react-native";
import { Dimensions } from "react-native";
import { StyleSheet, Text, View } from "react-native";
import { Sizing, Typography } from "../styles";
import PropTypes from "prop-types";
import { colors } from "../styles/theme";

const { width } = Dimensions.get("screen");

const HEADER_HEIGHt = 32;
const ITEM_HEIGHT = 45;
const TABLE_BORDER_RADIUS = Sizing.layout.x16;
const TABLE_COLUMN_WIDTH = width * 0.32;

const ScrollableTable = ({ data, headings, type }) => {
  return (
    <View style={styles.tableContainer}>
      <View style={styles.teamsTitle}>
        <View style={{ flexDirection: "row" }}>
          <Text style={[styles.headerText, { marginRight: 20 }]}>#</Text>
          <Text style={styles.headerText}>{type}</Text>
        </View>
      </View>
      <View style={styles.columnTitleContainer}>
        {data.map((item, index) => (
          <View
            key={`row-name-${index}`}
            style={[
              styles.columnTitle,
              {
                borderBottomLeftRadius: index === data.length - 1 ? 16 : 0,
              },
            ]}
          >
            <Text style={[styles.columnText, { marginHorizontal: 6 }]}>
              {index + 1}
            </Text>
            {/* Will be replaces with image */}
            {type === "player" ? (
              <View style={{ marginLeft: 6 }}>
                <Text
                  numberOfLines={1}
                  style={{
                    ...Typography.fontSize.x30,
                  }}
                >
                  {item.firstName}
                </Text>
                <Text
                  numberOfLines={1}
                  style={{
                    ...Typography.fontSize.x10,
                    color: colors.veryLightGrey,
                  }}
                >
                  {item.lastName}
                </Text>
              </View>
            ) : (
              <>
                <View style={styles.dummyImage} />
                <Text
                  style={{
                    ...styles.columnText,
                    flexGrow: 1,
                  }}
                >
                  {item.name}
                </Text>
              </>
            )}
          </View>
        ))}
      </View>

      <ScrollView
        horizontal
        style={styles.columns}
        showsHorizontalScrollIndicator={false}
        bounces={false}
      >
        {headings.map((item, i) => (
          <View key={`${item}-${i}`}>
            <View style={[styles.columnName, { alignItems: "center" }]}>
              <Text style={styles.headerText}>{item}</Text>
            </View>

            {data.map((data, index) => {
              if (typeof data[item] === "object" && item === "form") {
                return (
                  <View
                    key={`win-or-lose-${index}`}
                    style={[
                      styles.dataItem,
                      { flexDirection: "row", alignItems: "center" },
                    ]}
                  >
                    {data[item].map((item) => (
                      <Text
                        style={[
                          { color: item ? colors.success : colors.danger },
                          {
                            ...Typography.fontWeight.bold,
                            marginHorizontal: 6,
                            ...Typography.fontSize.x20,
                          },
                        ]}
                      >
                        {item ? "W" : "L"}
                      </Text>
                    ))}
                  </View>
                );
              }
              return (
                <View key={`data-item-${index}`} style={styles.dataItem}>
                  <Text style={styles.dataItemText}>{data[item]}</Text>
                </View>
              );
            })}
          </View>
        ))}
      </ScrollView>
    </View>
  );
};

ScrollableTable.defaultProps = {
  data: [],
  headings: [],
  type: "Teams",
};

ScrollableTable.propTypes = {
  data: PropTypes.array.isRequired,
  headings: PropTypes.array.isRequired,
  type: PropTypes.string,
};

export default ScrollableTable;

const styles = StyleSheet.create({
  headerText: {
    color: colors.white,
    ...Typography.fontSize.x15,
    textTransform: "capitalize",
  },
  columnText: {
    ...Typography.fontSize.x20,
    textTransform: "uppercase",
    color: colors.lightGrey,
  },
  columns: {
    position: "absolute",
    left: TABLE_COLUMN_WIDTH,
    right: 0,
  },
  columnName: {
    paddingHorizontal: 16,
    height: HEADER_HEIGHt,
    justifyContent: "center",
  },
  dataItem: {
    height: ITEM_HEIGHT,
    justifyContent: "center",
    borderBottomWidth: 1,
    borderBottomColor: colors.lightestGrey,
  },
  dataItemText: {
    textAlign: "center",
    ...Typography.fontSize.x20,
    paddingVertical: 8,
    color: colors.lightGrey,
  },
  columnTitle: {
    height: ITEM_HEIGHT,
    borderBottomWidth: 1,
    borderBottomColor: colors.lightestGrey,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 8,
    width: TABLE_COLUMN_WIDTH,
    backgroundColor: colors.white,
    borderRightWidth: 1,
    borderRightColor: colors.lightestGrey,
  },
  dummyImage: {
    width: 20,
    height: 15,
    backgroundColor: colors.lightGrey,
    borderRadius: Sizing.layout.x5,
    marginHorizontal: Sizing.layout.x8,
  },
  columnTitleContainer: {
    backgroundColor: colors.white,
    borderBottomRightRadius: TABLE_BORDER_RADIUS,
    borderBottomLeftRadius: TABLE_BORDER_RADIUS,
  },
  teamsTitle: {
    flexDirection: "row",
    backgroundColor: colors.grey,
    height: HEADER_HEIGHt,
    borderTopLeftRadius: TABLE_BORDER_RADIUS,
    borderTopRightRadius: TABLE_BORDER_RADIUS,
    padding: Sizing.layout.x8,
    alignItems: "center",
  },
  tableContainer: { position: "relative", marginVertical: Sizing.layout.x8 },
});
