import React, { useState } from "react";
import { TouchableOpacity } from "react-native";
import { ScrollView } from "react-native";
import { StyleSheet, Text, View } from "react-native";
import { Card, Divider } from "react-native-elements";
import { TouchableRipple } from "react-native-paper";
import { Sizing, Typography } from "../../styles";
import { cardStyles } from "../../styles/card";
import { colors } from "../../styles/theme";
import horizontalScroller from "../../styles/horizontalScroller";

const players = [
  { name: "Jaspriy Bumrah", type: "Fast Bowler", id: "1" },
  { name: "Jaspriy Bumrah", type: "Fast Bowler", id: "2" },
  { name: "Jaspriy Bumrah", type: "Fast Bowler", id: "3" },
  { name: "Jaspriy Bumrah", type: "Fast Bowler", id: "4" },
  { name: "Jaspriy Bumrah", type: "Fast Bowler", id: "5" },
  { name: "Jaspriy Bumrah", type: "Fast Bowler", id: "6" },
  { name: "Jaspriy Bumrah", type: "Fast Bowler", id: "7" },
  { name: "virat kohli", type: "Batsman", id: "8" },
  { name: "virat kohli", type: "Batsman", id: "9" },
  { name: "virat kohli", type: "Batsman", id: "10" },
  { name: "virat kohli", type: "Batsman", id: "11" },
  { name: "virat kohli", type: "Batsman", id: "12" },
  { name: "virat kohli", type: "Batsman", id: "13" },
];

const teams = ["csk", "mi", "srh", "kkr", "rcb", "rr", "dc", "kxip"];

const Squads = () => {
  const [currentIndex, setCurrentIndex] = useState(0);

  return (
    <>
      <ScrollView
        style={{ marginHorizontal: Sizing.layout.x8 }}
        horizontal
        showsHorizontalScrollIndicator={false}
      >
        {teams.map((team, index) => (
          <TouchableOpacity
            key={`team-${index}`}
            activeOpacity={0.7}
            onPress={() => setCurrentIndex(index)}
            disabled={index === currentIndex}
            style={[
              horizontalScroller.container,
              {
                backgroundColor:
                  index === currentIndex
                    ? colors.white
                    : "rgba(255,255,255,.5)",
                elevation: index === currentIndex ? Sizing.layout.x8 : 0,
              },
            ]}
          >
            <View
              style={[
                horizontalScroller.dummyImage,
                {
                  backgroundColor:
                    index === currentIndex
                      ? colors.primary
                      : colors.veryLightGrey,
                },
              ]}
            />
            <Text
              style={[
                horizontalScroller.title,
                {
                  color:
                    index === currentIndex
                      ? colors.primary
                      : colors.veryLightGrey,
                },
              ]}
            >
              {team}
            </Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
      <Card containerStyle={[cardStyles.cardContainer, { marginTop: 0 }]}>
        <View style={cardStyles.cardHeaderContainer}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <View style={styles.teamLogo} />
            <Text style={cardStyles.cardTitle}>Team Name</Text>
          </View>
        </View>
        <Divider />
        {players.map((player) => (
          <View key={player.id} style={styles.rowItem}>
            <Text style={[styles.text, { color: colors.lightGrey }]}>
              {player.name}
            </Text>
            <Text style={[styles.text, { color: colors.success }]}>
              {player.type}
            </Text>
          </View>
        ))}
      </Card>
    </>
  );
};

export default Squads;

const styles = StyleSheet.create({
  teamLogo: {
    height: 20,
    width: 20,
    backgroundColor: colors.veryLightGrey,
    borderRadius: Sizing.layout.x8,
    marginRight: Sizing.layout.x20,
  },
  text: {
    ...Typography.fontSize.x20,
    textTransform: "capitalize",
    ...Typography.fontWeight.semibold,
    letterSpacing: 0.8,
  },
  rowItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: Sizing.layout.x5,
    marginVertical: Sizing.layout.x2,
  },
});
