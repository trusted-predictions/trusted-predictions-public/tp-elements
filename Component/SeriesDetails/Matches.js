import React, { useState } from "react";
import { FlatList } from "react-native";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { Card, Divider } from "react-native-elements";
import { Sizing } from "../../styles";
import { cardStyles } from "../../styles/card";
import horizontalScroller from "../../styles/horizontalScroller";
import { percentage } from "../../styles/sizing";
import { colors } from "../../styles/theme";
import ScoreCardItems from "../Card/ScoreCardItems";

const schedule = [
  {
    nameOfLeague: "Name of league",
    team1: {
      name: "Australia",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
    },
    team2: {
      name: "India",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
    },
    date: "30 July",
    time: "10:30 AM",
    id: "1",
  },
  {
    nameOfLeague: "Name of league",
    team1: {
      name: "Australia",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
    },
    team2: {
      name: "India",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
    },
    date: "30 July",
    time: "10:30 AM",
    id: "2",
  },
  {
    nameOfLeague: "Name of league",
    team1: {
      name: "Australia",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
    },
    team2: {
      name: "India",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
    },
    date: "30 July",
    time: "10:30 AM",
    id: "4",
  },
  {
    nameOfLeague: "Name of league",
    team1: {
      name: "Australia",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
    },
    team2: {
      name: "India",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
    },
    date: "30 July",
    time: "10:30 AM",
    id: "5",
  },
  {
    nameOfLeague: "Name of league",
    team1: {
      name: "Australia",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
    },
    team2: {
      name: "India",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
    },
    date: "30 July",
    time: "10:30 AM",
    id: "6",
  },
  {
    nameOfLeague: "Name of league",
    team1: {
      name: "Australia",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
    },
    team2: {
      name: "India",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
    },
    date: "30 July",
    time: "10:30 AM",
    id: "7",
  },
  {
    nameOfLeague: "Name of league",
    team1: {
      name: "Australia",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
    },
    team2: {
      name: "India",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
    },
    date: "30 July",
    time: "10:30 AM",
    id: "8",
  },
  {
    nameOfLeague: "Name of league",
    team1: {
      name: "Australia",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
    },
    team2: {
      name: "India",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
    },
    date: "30 July",
    time: "10:30 AM",
    id: "9",
  },
  {
    nameOfLeague: "Name of league",
    team1: {
      name: "Australia",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
    },
    team2: {
      name: "India",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
    },
    date: "30 July",
    time: "10:30 AM",
    id: "10",
  },
  {
    nameOfLeague: "Name of league",
    team1: {
      name: "Australia",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
    },
    team2: {
      name: "India",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
    },
    date: "30 July",
    time: "10:30 AM",
    id: "11",
  },
];

const teams = ["all", "csk", "mi", "srh", "kkr", "rcb", "rr", "dc", "kxip"];

const Matches = () => {
  const [currentIndex, setCurrentIndex] = useState(0);
  return (
    <View style={{ flex: 1, marginVertical: 12 }}>
      <ScrollView
        style={{ marginHorizontal: Sizing.layout.x8 }}
        horizontal
        showsHorizontalScrollIndicator={false}
      >
        {teams.map((team, index) => (
          <TouchableOpacity
            key={`team-${index}`}
            activeOpacity={0.7}
            onPress={() => setCurrentIndex(index)}
            disabled={index === currentIndex}
            style={[
              horizontalScroller.container,
              {
                backgroundColor:
                  index === currentIndex
                    ? colors.white
                    : "rgba(255,255,255,.5)",
                elevation: index === currentIndex ? Sizing.layout.x8 : 0,
              },
            ]}
          >
            <View
              style={[
                horizontalScroller.dummyImage,
                {
                  backgroundColor:
                    index === currentIndex
                      ? colors.primary
                      : colors.veryLightGrey,
                },
              ]}
            />
            <Text
              style={[
                horizontalScroller.title,
                {
                  color:
                    index === currentIndex
                      ? colors.primary
                      : colors.veryLightGrey,
                },
              ]}
            >
              {team}
            </Text>
          </TouchableOpacity>
        ))}
      </ScrollView>

      <Card containerStyle={[cardStyles.cardContainer, { marginTop: 0 }]}>
        <View style={cardStyles.cardHeaderContainer}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text style={cardStyles.cardTitle}>Qualifiers</Text>
          </View>
        </View>
        <Divider />
        <FlatList
          data={schedule}
          style={percentage.h50}
          keyExtractor={(item) => item.id}
          bounces={false}
          showsVerticalScrollIndicator={false}
          renderItem={({ item }) => (
            <ScoreCardItems item={item} type="upcoming" />
          )}
        />
      </Card>
    </View>
  );
};

export default Matches;

const styles = StyleSheet.create({});
