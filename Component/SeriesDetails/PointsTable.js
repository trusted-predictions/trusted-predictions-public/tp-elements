import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Sizing } from "../../styles";
import ScrollableTable from "../ScrollableTable";

const data = [
  {
    id: "1",
    name: "mi",
    played: 14,
    won: 9,
    lost: 5,
    tied: 0,
    nr: 0,
    NetRR: +0.589,
    points: 9,
    form: [0, 1, 1, 0, 1],
  },
  {
    id: "2",
    name: "csk",
    played: 14,
    won: 8,
    lost: 6,
    tied: 0,
    nr: 0,
    NetRR: +0.489,
    points: 8,
    form: [1, , 0, 1, 1, 0],
  },
  {
    id: "3",
    name: "dc",
    played: 14,
    won: 8,
    lost: 6,
    tied: 0,
    nr: 0,
    NetRR: +0.189,
    points: 8,
    form: [1, 0, 1, 0, 1],
  },
  {
    id: "4",
    name: "srh",
    played: 14,
    won: 7,
    lost: 7,
    tied: 0,
    nr: 0,
    NetRR: +0.089,
    points: 7,
    form: [0, 1, 1, 1, 1],
  },
  {
    id: "5",
    played: 14,
    name: "kkr",
    won: 6,
    lost: 7,
    tied: 1,
    nr: 0,
    NetRR: +0.589,
    points: 7,
    form: [1, 0, 1, 0, 0],
  },
  {
    id: "6",
    played: 14,
    name: "kxip",
    won: 5,
    lost: 9,
    tied: 0,
    nr: 0,
    NetRR: -0.289,
    points: 5,
    form: [0, 0, 1, 0, 1],
  },
  {
    id: "7",
    name: "rr",
    played: 14,
    won: 4,
    lost: 10,
    tied: 0,
    nr: 0,
    NetRR: -0.589,
    points: 4,
    form: [0, 0, 1, 0, 1],
  },
  {
    id: "8",
    name: "rcb",
    played: 14,
    won: 4,
    lost: 9,
    tied: 1,
    nr: 0,
    NetRR: -0.889,
    points: 9,
    form: [0, 1, 0, 0, 0],
  },
];

const headings = [
  "played",
  "won",
  "lost",
  "tied",
  "nr",
  "NetRR",
  "points",
  "form",
];

const PointsTable = () => {
  return (
    <View
      style={{
        flex: 1,
        marginHorizontal: Sizing.layout.x8,
      }}
    >
      <ScrollableTable data={data} headings={headings} />
    </View>
  );
};

export default PointsTable;

const styles = StyleSheet.create({});
