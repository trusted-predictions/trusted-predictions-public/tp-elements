import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Sizing } from "../../styles";
import ScrollableTable from "../ScrollableTable";

const data = [
  {
    id: "1",
    firstName: "Mayank",
    lastName: "Agarwal",
    mat: 19,
    inns: 19,
    NO: 1,
    runs: 1126,
    HS: 127,
    Avg: 50.6,
    BF: 60,
    SR: 148.6,
    "100s": 1,
    "50s": 4,
    "4s": 29,
    "6s": 12,
  },
  {
    id: "2",
    firstName: "Mayank",
    lastName: "Agarwal",
    mat: 19,
    inns: 19,
    NO: 1,
    runs: 1126,
    HS: 127,
    Avg: 50.6,
    BF: 60,
    SR: 148.6,
    "100s": 1,
    "50s": 4,
    "4s": 29,
    "6s": 12,
  },
  {
    id: "3",
    firstName: "Mayank",
    lastName: "Agarwal",
    mat: 19,
    inns: 19,
    NO: 1,
    runs: 1126,
    HS: 127,
    Avg: 50.6,
    BF: 60,
    SR: 148.6,
    "100s": 1,
    "50s": 4,
    "4s": 29,
    "6s": 12,
  },
  {
    id: "4",
    firstName: "Mayank",
    lastName: "Agarwal",
    mat: 19,
    inns: 19,
    NO: 1,
    runs: 1126,
    HS: 127,
    Avg: 50.6,
    BF: 60,
    SR: 148.6,
    "100s": 1,
    "50s": 4,
    "4s": 29,
    "6s": 12,
  },
  {
    id: "5",
    firstName: "Mayank",
    lastName: "Agarwal",
    mat: 19,
    inns: 19,
    NO: 1,
    runs: 1126,
    HS: 127,
    Avg: 50.6,
    BF: 60,
    SR: 148.6,
    "100s": 1,
    "50s": 4,
    "4s": 29,
    "6s": 12,
  },
  {
    id: "6",
    firstName: "Mayank",
    lastName: "Agarwal",
    mat: 19,
    inns: 19,
    NO: 1,
    runs: 1126,
    HS: 127,
    Avg: 50.6,
    BF: 60,
    SR: 148.6,
    "100s": 1,
    "50s": 4,
    "4s": 29,
    "6s": 12,
  },
  {
    id: "7",
    firstName: "Mayank",
    lastName: "Agarwal",
    mat: 19,
    inns: 19,
    NO: 1,
    runs: 1126,
    HS: 127,
    Avg: 50.6,
    BF: 60,
    SR: 148.6,
    "100s": 1,
    "50s": 4,
    "4s": 29,
    "6s": 12,
  },
];

const headings = [
  "mat",
  "inns",
  "NO",
  "runs",
  "HS",
  "Avg",
  "BF",
  "SR",
  "100s",
  "50s",
  "4s",
  "6s",
];

const BattingStats = () => {
  return (
    <View style={{ flex: 1, marginHorizontal: Sizing.layout.x8 }}>
      <ScrollableTable headings={headings} data={data} type="player" />
    </View>
  );
};

export default BattingStats;

const styles = StyleSheet.create({});
