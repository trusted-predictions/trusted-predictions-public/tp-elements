import React from "react";
import { StyleSheet, Text, View } from "react-native";
import TwoTabs from "../TwoTabs";
import BattingStats from "./BattingStats";
import BowlingStats from "./BowlingStats";

const Stats = () => {
  return (
    <TwoTabs
      tab1name="Batting Stats"
      tab1={BattingStats}
      tab2={BowlingStats}
      tab2name="Bowling Stats"
      large
    />
  );
};

export default Stats;

const styles = StyleSheet.create({});
