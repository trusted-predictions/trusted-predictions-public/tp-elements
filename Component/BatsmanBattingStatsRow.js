import React from "react";
import { StyleSheet, Text, View } from "react-native";
import PropTypes from "prop-types";
import { Sizing, Typography } from "../styles";
import { colors } from "../styles/theme";
import { battingTableWidth } from "../styles/tableWidth";

const BatsmanBattingStatsRow = ({ data, showWicketTakers }) => {
  const { name, out, run, SR } = data;

  return (
    <View
      style={{
        flexDirection: "row",
        justifyContent: "space-between",
        marginVertical: showWicketTakers ? Sizing.layout.x4 : Sizing.layout.x8,
      }}
    >
      <Text
        numberOfLines={showWicketTakers ? 2 : 1}
        adjustsFontSizeToFit={!showWicketTakers}
        style={[
          styles.text,
          { width: battingTableWidth.nameWidth, textAlign: "left" },
        ]}
      >
        {name}
        <Text
          style={{
            ...Typography.fontSize.x10,
            color: out ? colors.danger : colors.success,
          }}
        >
          {showWicketTakers && `\n${out || "not out"}`}
        </Text>
      </Text>
      <View
        style={{
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <Text style={[{ width: battingTableWidth.runsWidth }, styles.text]}>
          {run}
        </Text>
        <Text
          style={[{ width: battingTableWidth.boundariesWidth }, styles.text]}
        >
          {data["4s"]}
        </Text>
        <Text
          style={[{ width: battingTableWidth.boundariesWidth }, styles.text]}
        >
          {data["6s"]}
        </Text>
        <Text
          style={[
            styles.text,
            { width: battingTableWidth.strikeRateWidth, textAlign: "right" },
          ]}
        >
          {SR}
        </Text>
      </View>
    </View>
  );
};

BatsmanBattingStatsRow.defaultProps = {
  showWicketTakers: false,
};

BatsmanBattingStatsRow.propTypes = {
  showWicketTakers: PropTypes.bool,
  data: PropTypes.object.isRequired,
};

export default BatsmanBattingStatsRow;

const styles = StyleSheet.create({
  text: {
    textAlign: "center",
  },
});
