import React, { useRef } from "react";
import { Dimensions, Animated, Image, StyleSheet, View } from "react-native";
import PropTypes from "prop-types";
import { colors } from "../styles/theme";
import * as Sizing from "../styles/sizing";

const { width } = Dimensions.get("window");
const SLIDER_HEIGHT = 140;
const INDICATOR_WIDTH = Sizing.layout.x10;

const ImageSlider = ({ items }) => {
  const scrollX = useRef(new Animated.Value(0)).current;

  const Indicator = ({ scrollX }) => {
    return (
      <View style={styles.indicatorContainer}>
        {items.map((_, i) => {
          const inputRange = [(i - 1) * width, i * width, (i + 1) * width];
          const scale = scrollX.interpolate({
            inputRange,
            outputRange: [0.8, 1.3, 0.8],
            extrapolate: "clamp",
          });
          const opacity = scrollX.interpolate({
            inputRange,
            outputRange: [0.6, 1, 0.6],
            extrapolate: "clamp",
          });
          return (
            <Animated.View
              key={`indicator-${i}`}
              style={[
                styles.indicator,
                {
                  opacity,
                  transform: [{ scale }],
                },
              ]}
            />
          );
        })}
      </View>
    );
  };

  return (
    <View>
      <Animated.FlatList
        data={items}
        style={{ maxHeight: SLIDER_HEIGHT }}
        keyExtractor={(item) => item.id}
        showsHorizontalScrollIndicator={false}
        horizontal
        decelerationRate="fast"
        pagingEnabled
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: false }
        )}
        renderItem={({ item }) => {
          return (
            <View style={styles.imageContainer}>
              <Image
                source={{
                  uri: item.image,
                }}
                style={styles.image}
                resizeMode="cover"
              />
            </View>
          );
        }}
      />
      <Indicator scrollX={scrollX} />
    </View>
  );
};

export default ImageSlider;

ImageSlider.propTypes = {
  items: PropTypes.array.isRequired,
};

const styles = StyleSheet.create({
  indicator: {
    height: INDICATOR_WIDTH,
    width: INDICATOR_WIDTH,
    borderRadius: INDICATOR_WIDTH / 2,
    backgroundColor: colors.lightGrey,
    margin: Sizing.layout.x8,
  },
  indicatorContainer: { flexDirection: "row", alignSelf: "center" },
  imageContainer: {
    width: width,
    height: SLIDER_HEIGHT,
    paddingHorizontal: Sizing.layout.x8,
    paddingVertical: Sizing.layout.x8,
  },
  image: { height: "100%", width: "100%", borderRadius: Sizing.layout.x8 },
});
