import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Card } from "react-native-elements";
import { cardStyles } from "../../styles/card";
cardStyles;
import { Sizing, Typography } from "../../styles";
import { colors } from "../../styles/theme";
import { Bar } from "react-native-progress";

const playersList = [
  { name: "Virat Kohli", country: "IND", percentage: 63 },
  { name: "Rohit Sharma", country: "IND", percentage: 60 },
  { name: "Steve Smith", country: "AUS", percentage: 56 },
  { name: "David Warner", country: "AUS", percentage: 52 },
  { name: "Arron Finch", country: "AUS", percentage: 44 },
  { name: "K L Rahul", country: "IND", percentage: 40 },
  { name: "Ravindra Jadeja", country: "IND", percentage: 38 },
  { name: "Adam Zampa", country: "AUS", percentage: 34 },
];

const PERCENTAGE_INDICATOR_WIDTH = 30;

const TopPerformersCard = () => {
  return (
    <Card containerStyle={cardStyles.cardContainer}>
      {/* Card Header */}
      <View style={cardStyles.cardHeaderContainer}>
        <Text style={cardStyles.cardTitle}>Top performers</Text>
      </View>
      {/* Card Items */}
      <View style={{ flexDirection: "row" }}>
        <View style={{ flex: 1 }} />
        <View style={{ flex: 1, alignItems: "center" }}>
          <Text style={styles.contributionHeader}>Contribution</Text>
        </View>
      </View>

      {playersList.map((item, index) => {
        return (
          <View key={`player-${index}`} style={styles.listItemContainer}>
            <View style={{ flex: 3 }}>
              <Text
                style={styles.playerName}
                numberOfLines={1}
              >{`${item.name} (${item.country})`}</Text>
            </View>
            <View
              style={{
                flex: 4,
                alignItems: "center",
                flexDirection: "row",
              }}
            >
              <Bar
                progress={item.percentage / 100}
                width={null}
                height={12}
                style={{ flexGrow: 1 }}
                borderRadius={8}
                color={colors.primary}
                borderColor={colors.primary}
              />
              <Text style={styles.percentageText}>{`${item.percentage}%`}</Text>
            </View>
          </View>
        );
      })}
      {/* Card Items */}
    </Card>
  );
};

export default TopPerformersCard;

const styles = StyleSheet.create({
  percentageText: {
    marginLeft: Sizing.layout.x5,
    textAlign: "center",
    color: colors.veryLightGrey,
  },
  playerName: {
    ...Typography.fontSize.x15,
    color: colors.grey,
  },
  listItemContainer: { flexDirection: "row", marginVertical: Sizing.layout.x8 },
  contributionHeader: {
    ...Typography.fontSize.x10,
    color: colors.lightGrey,
    marginRight: PERCENTAGE_INDICATOR_WIDTH,
  },
});
