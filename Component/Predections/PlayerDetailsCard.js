import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { Sizing, Typography } from "../../styles";
import { borderRadius } from "../../styles/outlines";
import { colors } from "../../styles/theme";

const CARD_WIDTH = 110;

const PlayerDetailsCard = () => {
  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        resizeMode="contain"
        source={require("../../assets/images/player_half.png")}
      />
      <View style={styles.overlay} />
      <Text
        style={{
          color: colors.white,
          ...Typography.fontSize.x20,
        }}
      >
        VIRAT
      </Text>
      <Text style={styles.smallText}>Kohli</Text>
      <Text style={{ color: colors.white, marginVertical: Sizing.layout.x3 }}>
        <Text style={{ color: colors.danger, ...Typography.fontSize.x15 }}>
          ODI
        </Text>{" "}
        Career
      </Text>
      <Text style={styles.smallText}>Matches : 150</Text>
      <Text style={styles.smallText}>Runs : 12805</Text>
      <Text style={styles.smallText}>100s : 41</Text>
      <Text style={styles.smallText}>50s : 70</Text>
      <Text style={styles.smallText}>Avg : 52.5</Text>
      <Text style={styles.smallText}>SR : 98.07</Text>
    </View>
  );
};

export default PlayerDetailsCard;

const styles = StyleSheet.create({
  smallText: {
    color: colors.white,
    fontSize: 11,
    marginBottom: Sizing.layout.x3,
  },
  container: {
    backgroundColor: colors.primary,
    width: CARD_WIDTH,
    marginHorizontal: Sizing.layout.x4,
    borderRadius: borderRadius.mid,
    position: "relative",
    alignItems: "flex-end",
    padding: Sizing.layout.x4,
    overflow: "hidden",
  },
  image: {
    position: "absolute",
    bottom: 0,
    right: 25,
    height: "100%",
    width: CARD_WIDTH,
    alignSelf: "flex-start",
  },
  overlay: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: colors.darkOverLay1,
  },
});
