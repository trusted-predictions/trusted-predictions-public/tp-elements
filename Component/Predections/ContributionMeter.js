import React from "react";
import { View } from "react-native";
import PropTypes from "prop-types";
import { Sizing } from "../../styles";
import { colors } from "../../styles/theme";

const ContributionMeter = ({ width, height, percentage }) => {
  return (
    <View
      style={{
        backgroundColor: colors.lightestGrey,
        width,
        height,
        borderRadius: Sizing.layout.x5,
      }}
    >
      <View
        style={{
          width: (width / 100) * percentage,
          backgroundColor: colors.primary,
          height,
          borderRadius: Sizing.layout.x5,
        }}
      />
    </View>
  );
};

ContributionMeter.propTypes = {
  width: PropTypes.number.isRequired,
  percentage: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
};

export default ContributionMeter;
