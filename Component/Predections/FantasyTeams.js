import React from "react";
import { Text, View } from "react-native";
import { Card, Divider } from "react-native-elements";
import { cardStyles } from "../../styles/card";
cardStyles;
import FantasyTeamListItem from "./FantasyTeamListItem";

const FantasyTeamList = () => {
  return (
    <Card containerStyle={cardStyles.cardContainer}>
      {/* Card Header */}
      <View style={cardStyles.cardHeaderContainer}>
        <Text style={cardStyles.cardTitle}>Fantasy Teams</Text>
      </View>
      <Divider />
      {/* Card Items */}
      {[...Array(10).keys()].map((item) => (
        <FantasyTeamListItem item={{}} key={item.toString()} />
      ))}
    </Card>
  );
};

export default FantasyTeamList;
