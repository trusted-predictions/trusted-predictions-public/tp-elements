import React from "react";
import { FlatList, StyleSheet, Text, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";
import { colors } from "../../styles/theme";
import shadow from "../../styles/shadow";
import { Sizing, Typography } from "../../styles";

const DATE_SLIDER_HEIGHT = 40;

const DateSlider = ({ dates, activeIndex = 2 }) => {
  return (
    <FlatList
      data={dates}
      bounces={false}
      contentContainerStyle={{ alignItems: "center" }}
      style={styles.listStyle}
      keyExtractor={(_, id) => `date-${id}`}
      horizontal
      showsHorizontalScrollIndicator={false}
      renderItem={({ item, index }) => {
        const backgroundColor =
          activeIndex === index ? colors.primary : colors.white;
        const color = activeIndex === index ? colors.white : colors.black;
        return (
          <TouchableOpacity
            onPress={() => {}}
            activeOpacity={0.6}
            style={[styles.lisItem, { backgroundColor }]}
          >
            <Text numberOfLines={1} style={[styles.lisItemText, { color }]}>
              {item}
            </Text>
          </TouchableOpacity>
        );
      }}
    />
  );
};

DateSlider.propTypes = {
  dates: PropTypes.array.isRequired,
};

export default DateSlider;

const styles = StyleSheet.create({
  listStyle: {
    backgroundColor: colors.white,
    height: DATE_SLIDER_HEIGHT,
    ...shadow.mildShadow,
  },
  lisItem: {
    marginHorizontal: Sizing.layout.x8,
    paddingHorizontal: Sizing.layout.x8,
    paddingVertical: Sizing.layout.x3,
    borderRadius: Sizing.layout.x5,
    ...shadow.lightShadow,
  },
  lisItemText: {
    ...Typography.fontSize.x15,
    ...Typography.fontWeight.semibold,
    textTransform: "capitalize",
  },
});
