import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { Card, Divider } from "react-native-elements";
import { Sizing, Typography } from "../../styles";
import { cardStyles } from "../../styles/card";
import { transparent } from "../../styles/colors";
import { colors } from "../../styles/theme";

const WhoWillWin = () => {
  return (
    <Card containerStyle={cardStyles.cardContainer}>
      {/* Card Header */}
      <View style={cardStyles.cardHeaderContainer}>
        <Text style={cardStyles.cardTitle}>Winner Team</Text>
      </View>
      <Divider />
      {/* Card Items */}
      <View style={{ flexDirection: "row" }}>
        <View style={styles.leftContainer}>
          <Image
            source={require("../../assets/images/team.png")}
            resizeMode="contain"
            style={styles.winningTeamImage}
          />
        </View>
        <View style={styles.rightContainer}>
          <Text style={styles.winningTeamName}>Perth Scorchers</Text>
          <Text style={{ color: colors.lightGrey }}>
            Perth Scorchers will win the match.
          </Text>
        </View>
      </View>
    </Card>
  );
};

export default WhoWillWin;

const styles = StyleSheet.create({
  leftContainer: {
    borderWidth: 0,
    borderRightWidth: 0.5,
    borderRightColor: transparent.lightGray,
    flex: 2,
    marginTop: Sizing.layout.x8,
    alignItems: "center",
  },
  rightContainer: {
    flex: 3,
    paddingHorizontal: Sizing.layout.x8,
    marginTop: Sizing.layout.x8,
  },
  winningTeamImage: { height: 50, width: 50, marginLeft: -15 },
  winningTeamName: {
    color: colors.success,
    ...Typography.fontWeight.bold,
    ...Typography.fontSize.x20,
    marginBottom: Sizing.layout.x5,
  },
});
