import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { Divider, Icon } from "react-native-elements";
import PropTypes from "prop-types";
import { transparent } from "../../styles/colors";
import { colors } from "../../styles/theme";
import { fontSize } from "../../styles/typography";
// Styles
import * as Sizing from "../../styles/sizing";

const FantasyTeamListItem = ({ item }) => {
  return (
    <View style={{ marginVertical: Sizing.layout.x5 }}>
      <View style={{ flexDirection: "row" }}>
        <View style={styles.teamNamesContainer}>
          <View style={styles.teamName}>
            <Image
              source={{
                uri:
                  "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
              }}
              resizeMode="contain"
              style={styles.flagImage}
            />
            <Text style={[styles.nameText, { color: colors.lightGrey }]}>
              Australia
            </Text>
          </View>

          <View style={styles.teamName}>
            <Image
              source={{
                uri:
                  "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
              }}
              resizeMode="contain"
              style={styles.flagImage}
            />
            <Text style={[styles.nameText, { color: colors.lightGrey }]}>
              India
            </Text>
          </View>
        </View>

        <View style={styles.rightContainer}>
          <View style={styles.detailsContainer}>
            <Text
              style={[
                fontSize.x15,
                { marginLeft: Sizing.layout.x5, color: colors.veryLightGrey },
              ]}
            >
              5 Players
            </Text>
            <Icon
              type="material-community"
              name="bell-ring"
              background={colors.grey}
              containerStyle={{ marginLeft: "auto" }}
              style={styles.icon}
              color={colors.grey}
              size={Sizing.icons.x5}
            />
          </View>
          <View style={styles.detailsContainer}>
            <Text
              style={[
                fontSize.x15,
                {
                  marginLeft: Sizing.layout.x5,
                  color: colors.veryLightGrey,
                },
              ]}
            >
              6 Players
            </Text>

            <Icon
              type="ionicon"
              name="md-share-social"
              background={colors.grey}
              containerStyle={{ marginLeft: "auto" }}
              style={[styles.icon]}
              color={colors.grey}
              size={Sizing.icons.x5}
            />
          </View>
        </View>
      </View>
      <Divider />
    </View>
  );
};

FantasyTeamListItem.propTypes = {
  item: PropTypes.object.isRequired,
};

export default FantasyTeamListItem;

const styles = StyleSheet.create({
  icon: {
    backgroundColor: transparent.lightGray,
    height: Sizing.layout.x22,
    width: Sizing.layout.x22,
    borderRadius: Sizing.layout.x10,
    justifyContent: "center",
    alignItems: "center",
  },
  teamNamesContainer: {
    borderWidth: 0,
    borderRightWidth: 0.5,
    borderRightColor: transparent.lightGray,
    flex: 1,
    marginVertical: Sizing.layout.x8,
  },
  teamName: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: Sizing.layout.x3,
  },
  flagImage: {
    height: Sizing.layout.x20,
    width: Sizing.layout.x20,
  },
  nameText: {
    ...fontSize.x20,
    marginLeft: Sizing.layout.x8,
  },
  detailsContainer: {
    flexDirection: "row",
    width: "100%",
    marginVertical: Sizing.layout.x3,
    alignItems: "center",
  },
  rightContainer: {
    flex: 1,
    paddingHorizontal: Sizing.layout.x8,
    marginVertical: Sizing.layout.x8,
  },
});
