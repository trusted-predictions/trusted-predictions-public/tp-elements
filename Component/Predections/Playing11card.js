import React from "react";
import { FlatList, Text, View } from "react-native";
import { Card } from "react-native-elements";
import { cardStyles } from "../../styles/card";
import PlayerDetailsCard from "./PlayerDetailsCard";
cardStyles;

const Playing11card = () => {
  return (
    <Card containerStyle={cardStyles.cardContainer}>
      {/* Card Header */}
      <View style={cardStyles.cardHeaderContainer}>
        <Text style={cardStyles.cardTitle}>Playing 11</Text>
      </View>
      {/* Card Items */}
      <FlatList
        horizontal
        showsHorizontalScrollIndicator={false}
        bounces={false}
        data={[...Array(11).keys()]}
        keyExtractor={(item) => item.toString()}
        renderItem={() => <PlayerDetailsCard />}
      />
    </Card>
  );
};

export default Playing11card;
