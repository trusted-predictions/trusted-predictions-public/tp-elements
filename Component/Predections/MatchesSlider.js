import React from "react";
import {
  StyleSheet,
  Text,
  FlatList,
  TouchableOpacity,
  View,
  Image,
} from "react-native";
import PropTypes from "prop-types";
import { colors } from "../../styles/theme";
import { Sizing, Typography } from "../../styles";
import shadow from "../../styles/shadow";

const MATCHES_SLIDER_HEIGHT = 88;

const MatchesSlider = ({ items, activeIndex = 0 }) => {
  return (
    <FlatList
      data={items}
      bounces={false}
      contentContainerStyle={{ alignItems: "center" }}
      style={styles.listStyle}
      keyExtractor={(_, id) => `match-${id}`}
      horizontal
      showsHorizontalScrollIndicator={false}
      renderItem={({ item, index }) => {
        const borderColor =
          activeIndex === index ? colors.primary : colors.white;
        return (
          <TouchableOpacity
            onPress={() => {}}
            activeOpacity={0.8}
            style={[styles.lisItem, { borderColor }]}
          >
            <Text style={styles.primaryText}>India tour of australia</Text>
            <View style={styles.teamNameContainer}>
              <Image
                source={{
                  uri:
                    "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
                }}
                resizeMode="contain"
                style={styles.flagImage}
              />
              <Text style={styles.teamName}>AUS</Text>
              <Text style={styles.versusLogo}>VS</Text>
              <Text style={styles.teamName}>IND</Text>
              <Image
                source={{
                  uri:
                    "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
                }}
                resizeMode="contain"
                style={styles.flagImage}
              />
            </View>
            <Text style={styles.secondaryText}>Aug 31 | 4:30 PM</Text>
          </TouchableOpacity>
        );
      }}
    />
  );
};

MatchesSlider.propTypes = {
  items: PropTypes.array.isRequired,
};

export default MatchesSlider;

const styles = StyleSheet.create({
  listStyle: {
    height: MATCHES_SLIDER_HEIGHT,
    marginTop: Sizing.layout.x8,
  },
  lisItem: {
    marginHorizontal: Sizing.layout.x10,
    paddingHorizontal: Sizing.layout.x10,
    paddingVertical: Sizing.layout.x5,
    borderRadius: Sizing.layout.x5,
    ...shadow.mildShadow,
    backgroundColor: colors.white,
    alignItems: "center",
    borderWidth: Sizing.layout.x2,
  },
  primaryText: {
    color: colors.success,
    ...Typography.fontSize.x15,
  },
  secondaryText: {
    color: colors.veryLightGrey,
    ...Typography.fontSize.x10,
  },
  flagImage: {
    height: Sizing.layout.x30,
    width: Sizing.layout.x30,
  },
  versusLogo: {
    color: colors.danger,
    ...Typography.fontSize.x10,
    ...Typography.fontWeight.bold,
  },
  teamName: {
    ...Typography.fontSize.x20,
    marginHorizontal: 6,
    color: colors.black,
    ...Typography.fontWeight.semibold,
  },
  teamNameContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginVertical: Sizing.layout.x3,
  },
});
