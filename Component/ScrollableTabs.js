import { StatusBar } from "expo-status-bar";
import React, { useMemo, useState } from "react";
import {
  Animated,
  Dimensions,
  StyleSheet,
  View,
  TouchableOpacity,
} from "react-native";
import { colors } from "../styles/theme";
import PropTypes from "prop-types";
import { Sizing, Typography } from "../styles";
import { Text } from "react-native";
import { ScrollView } from "react-native";
import { FlatList } from "react-native";

const { width } = Dimensions.get("screen");

const TAB_HEADER_MARGIN = 0;
const TAB_HEADER_HEIGHT = 45;
const ACTIVE_TAB_INDICATOR_HEIGHT = 5;

const ScrollableTabs = ({
  tabs,
  header,
  headerTitle,
  scrollable,
  scrollEnabled,
}) => {
  const scrollX = React.useRef(new Animated.Value(0)).current;
  const [headerWidth, setHeaderWidth] = useState(0);

  const headerScrollRef = React.useRef();
  const ref = React.useRef();

  const onHeaderItemPress = React.useCallback(
    (x) => {
      headerScrollRef?.current?.scrollTo({ x, animated: true });
    },
    [headerScrollRef]
  );

  const tabHeaderWidth = useMemo(
    () => (width - headerWidth - tabs.length * TAB_HEADER_MARGIN) / tabs.length,
    [headerWidth, tabs.length]
  );

  const scrollableMinWidth = useMemo(() => width / tabs.length, [tabs.length]);

  const onItemPress = React.useCallback(
    (itemIndex) => {
      ref?.current?.scrollToOffset({
        offset: itemIndex * width,
      });
      headerTitle && onHeaderItemPress((itemIndex * scrollableMinWidth) / 2);
    },
    [ref]
  );

  const translateX = scrollX.interpolate({
    inputRange: [0, width],
    outputRange: [0, tabHeaderWidth],
  });

  return (
    <View style={styles.container}>
      <StatusBar hidden />
      {header && header()}
      <View style={styles.tab}>
        {headerTitle !== "" && (
          <View
            onLayout={({
              nativeEvent: {
                layout: { width },
              },
            }) => setHeaderWidth(width)}
            style={{ justifyContent: "center" }}
          >
            <Text
              style={{
                ...Typography.fontSize.x40,
                ...Typography.fontWeight.bold,
                marginHorizontal: Sizing.layout.x16,
              }}
            >
              {headerTitle}
            </Text>
          </View>
        )}
        {scrollable ? (
          <ScrollView
            horizontal
            ref={headerScrollRef}
            showsHorizontalScrollIndicator={false}
          >
            {tabs.map((tab, index) => {
              const textColor = scrollX.interpolate({
                inputRange: [
                  (index - 1) * width,
                  index * width,
                  (index + 1) * width,
                ],
                outputRange: [
                  colors.lightGrey,
                  colors.primary,
                  colors.lightGrey,
                ],
                extrapolate: "clamp",
              });
              const borderColor = scrollX.interpolate({
                inputRange: [
                  (index - 1) * width,
                  index * width,
                  (index + 1) * width,
                ],
                outputRange: [colors.white, colors.primary, colors.white],
                extrapolate: "clamp",
              });
              return (
                <TouchableOpacity
                  key={`tab-header-${index}`}
                  activeOpacity={0.7}
                  onPress={() => onItemPress(index)}
                  style={[
                    styles.tabScrollableHeader,
                    { minWidth: scrollableMinWidth },
                  ]}
                >
                  <Animated.Text
                    numberOfLines={1}
                    style={[
                      {
                        ...Typography.fontSize.x15,
                        textTransform: "capitalize",
                      },
                      { color: textColor },
                    ]}
                  >
                    {tab.title}
                  </Animated.Text>
                  <Animated.View
                    style={[
                      styles.scrollableIndicator,
                      {
                        backgroundColor: borderColor,
                      },
                    ]}
                  />
                </TouchableOpacity>
              );
            })}
          </ScrollView>
        ) : (
          tabs.map((tab, index) => {
            const textColor = scrollX.interpolate({
              inputRange: [
                (index - 1) * width,
                index * width,
                (index + 1) * width,
              ],
              outputRange: [colors.lightGrey, colors.primary, colors.lightGrey],
              extrapolate: "clamp",
            });
            return (
              <TouchableOpacity
                key={`tab-header-${index}`}
                activeOpacity={0.7}
                onPress={() => onItemPress(index)}
                style={[
                  styles.tabHeader,
                  {
                    width: tabHeaderWidth,
                  },
                ]}
              >
                <Animated.Text
                  numberOfLines={1}
                  style={[
                    {
                      ...Typography.fontSize.x15,
                      textTransform: "capitalize",
                    },
                    { color: textColor },
                  ]}
                >
                  {tab.title}
                </Animated.Text>
              </TouchableOpacity>
            );
          })
        )}
        {!scrollable && (
          <Animated.View
            style={[
              styles.indicator,
              {
                width: tabHeaderWidth,
                transform: [{ translateX }],
                marginLeft: headerWidth,
              },
            ]}
          />
        )}
      </View>

      <Animated.FlatList
        data={tabs}
        ref={ref}
        scrollEnabled={scrollEnabled}
        onMomentumScrollEnd={({
          nativeEvent: {
            contentOffset: { x },
          },
        }) => {
          onHeaderItemPress(((x / width) * scrollableMinWidth) / 2);
        }}
        keyExtractor={(_, index) => `tab-${index}`}
        horizontal
        showsHorizontalScrollIndicator={false}
        pagingEnabled
        bounces={false}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: false }
        )}
        renderItem={({ item }) => (
          <ScrollView
            bounces={false}
            showsVerticalScrollIndicator={false}
            style={{ width }}
          >
            {item.tab()}
          </ScrollView>
        )}
      />
    </View>
  );
};

ScrollableTabs.defaultProps = {
  header: null,
  headerTitle: "",
  scrollable: false,
  scrollEnabled: true,
};

ScrollableTabs.propTypes = {
  tabs: PropTypes.array.isRequired,
  header: PropTypes.func,
  headerTitle: PropTypes.string,
  scrollable: PropTypes.bool,
  scrollEnabled: PropTypes.bool,
};

export default ScrollableTabs;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.lightestGrey,
    alignItems: "center",
    justifyContent: "center",
  },
  tab: {
    height: TAB_HEADER_HEIGHT,
    width,
    backgroundColor: colors.white,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  tabHeader: {
    marginVertical: 4,
    marginHorizontal: TAB_HEADER_MARGIN,
    justifyContent: "center",
    alignItems: "center",
  },
  tabScrollableHeader: {
    height: "100%",
    marginHorizontal: TAB_HEADER_MARGIN,
    paddingHorizontal: Sizing.layout.x10,
    position: "relative",
    justifyContent: "center",
    alignItems: "center",
  },
  indicator: {
    height: ACTIVE_TAB_INDICATOR_HEIGHT,
    borderRadius: 2,
    backgroundColor: colors.primary,
    position: "absolute",
    bottom: 0,
    left: 0,
  },
  scrollableIndicator: {
    height: ACTIVE_TAB_INDICATOR_HEIGHT,
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
  },
});
