import React from 'react'
import { View, StyleSheet, ScrollView } from 'react-native';
import AvailBalanceRender from './CoinManagement/AvailBalanceRender';
import ReferPage from './CoinManagement/ReferPage';
import WatchVideoRender from './CoinManagement/WatchVideoRender';
import { Sizing, Outlines, Colors, Typography, Theme } from "../styles/index";
function EarnCoinsRender() {
    return (
        <View style={styles.Container}>
            <ScrollView showsVerticalScrollIndicator={false} >
                <View style={styles.Divider}>
                    <AvailBalanceRender />
                </View>
                <View style={styles.Divider}> 
                    <ReferPage />
                </View>
                <View style={styles.Divider}> 
                    <WatchVideoRender />
                </View>
            </ScrollView>
        </View>
    )
}

export default EarnCoinsRender

const styles = StyleSheet.create({
    Divider:{
        marginTop:Sizing.x20
    },
    Container:{
        marginTop:60,
    }
});