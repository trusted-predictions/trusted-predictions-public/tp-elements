import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { bowlingTableWidth } from "../styles/tableWidth";

const BowlerBowlingStatsRow = ({ data }) => {
  return (
    <View style={styles.row}>
      <Text
        numberOfLines={1}
        adjustsFontSizeToFit
        style={[
          styles.text,
          { width: bowlingTableWidth.nameWidth, textAlign: "left" },
        ]}
      >
        {data.name}
      </Text>
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Text style={[{ width: bowlingTableWidth.over }, styles.text]}>
          {data.over}
        </Text>
        <Text style={[{ width: bowlingTableWidth.small }, styles.text]}>
          {data.m}
        </Text>
        <Text style={[{ width: bowlingTableWidth.small }, styles.text]}>
          {data.r}
        </Text>
        <Text style={[{ width: bowlingTableWidth.mid }, styles.text]}>
          {data.eco}
        </Text>
        <Text style={[{ width: bowlingTableWidth.small }, styles.text]}>
          {data.w}
        </Text>
      </View>
    </View>
  );
};

export default BowlerBowlingStatsRow;

const styles = StyleSheet.create({
  text: {
    textAlign: "center",
  },
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 8,
  },
});
