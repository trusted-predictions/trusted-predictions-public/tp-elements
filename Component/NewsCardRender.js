import React from 'react'
import { View, StyleSheet, ScrollView } from 'react-native';
import { Sizing } from "../styles/index";
import TrendingTopicsRender from './NewsCard/TrendingTopicsRender';
import FeaturedRender from './NewsCard/FeaturedRender';
import LatestNewsRender from './NewsCard/LatestNewsRender';

function NewsCardRender() {
    return (
        <View style={styles.Container}>
            <ScrollView showsVerticalScrollIndicator={false} >
                <TrendingTopicsRender />
                <View style={styles.Divider}>
                    <FeaturedRender />
                </View>
                <View style={styles.Divider}> 
                    <LatestNewsRender/>
                </View>
            </ScrollView>
        </View>
    )
}

export default NewsCardRender

const styles = StyleSheet.create({
    Divider:{
        marginTop:Sizing.x20
    },
    Container:{
        marginTop:60,
    }
});