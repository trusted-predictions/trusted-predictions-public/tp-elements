import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Card } from "react-native-elements";
import { TouchableRipple } from "react-native-paper";
import { cardStyles } from "../styles/card";
import ScoreCardItems from "./Card/ScoreCardItems";

const upcomingMatches = [
  {
    nameOfLeague: "Name of league",
    team1: {
      name: "Australia",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
    },
    team2: {
      name: "India",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
    },
    date: "30 July",
    time: "10:30 AM",
    id: "1",
  },
  {
    nameOfLeague: "Name of league",
    team1: {
      name: "Australia",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
    },
    team2: {
      name: "India",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
    },
    date: "30 July",
    time: "10:30 AM",
    id: "2",
  },
  {
    nameOfLeague: "Name of league",
    team1: {
      name: "Australia",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/jSgw5z0EPOLzdUi-Aomq7Q_48x48.png",
    },
    team2: {
      name: "India",
      image:
        "https://ssl.gstatic.com/onebox/media/sports/logos/mlXOOB9HXxLpoeS2dHSgGA_48x48.png",
    },
    date: "30 July",
    time: "10:30 AM",
    id: "3",
  },
];

const UpcomingMatches = () => {
  return (
    <Card containerStyle={cardStyles.cardContainer}>
      {/* Card Header */}
      <View style={cardStyles.cardHeaderContainer}>
        <Text style={cardStyles.cardTitle}>Upcoming Matches</Text>
        <TouchableRipple onPress={() => {}} style={cardStyles.cardRight}>
          <Text style={cardStyles.cardRightText}>View All</Text>
        </TouchableRipple>
      </View>
      {/* Card Items */}
      {upcomingMatches.map((item) => (
        <ScoreCardItems type="upcoming" item={item} key={item.id} />
      ))}
    </Card>
  );
};

export default UpcomingMatches;

const styles = StyleSheet.create({});
