import React, { useRef } from "react";
import {
  Dimensions,
  Animated,
  Image,
  StyleSheet,
  View,
  Text,
} from "react-native";
import PropTypes from "prop-types";
import { Sizing, Typography } from "../../styles";
import { colors } from "../../styles/theme";
import { transparent } from "../../styles/colors";

const { width } = Dimensions.get("window");
const IMAGE_HEIGHT = 220;
const IMAGE_WIDTH = width - 32 - 24;

const DOT_SIZE = 12;

const Pagination = ({ scrollX, items }) => {
  const inputRange = [-IMAGE_WIDTH, 0, IMAGE_WIDTH];
  const translateX = scrollX.interpolate({
    inputRange,
    outputRange: [-DOT_SIZE, 0, DOT_SIZE],
  });

  return (
    <View style={[styles.pagination]}>
      <Animated.View
        style={[
          styles.paginationIndicator,
          {
            position: "absolute",
            transform: [{ translateX }],
          },
        ]}
      />
      {items.map((item, index) => {
        const inputRange = [
          IMAGE_WIDTH * (index - 1),
          IMAGE_WIDTH * index,
          IMAGE_WIDTH * (index + 1),
        ];
        const backgroundColor = scrollX.interpolate({
          inputRange,
          outputRange: [
            colors.lightestGrey,
            colors.primary,
            colors.lightestGrey,
          ],
          extrapolate: "clamp",
        });
        return (
          <View key={item.toString()} style={styles.paginationDotContainer}>
            <Animated.View
              style={[styles.paginationDot, { backgroundColor }]}
            />
          </View>
        );
      })}
    </View>
  );
};

const ImageSlider = ({ items }) => {
  const scrollX = useRef(new Animated.Value(0)).current;

  return (
    <View>
      <Animated.FlatList
        data={items}
        keyExtractor={(item) => item.toString()}
        showsHorizontalScrollIndicator={false}
        horizontal
        decelerationRate="fast"
        snapToInterval={IMAGE_WIDTH}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: scrollX } } }],
          { useNativeDriver: false }
        )}
        renderItem={({ item }) => {
          return (
            <View style={styles.imageContainer}>
              <Image
                source={require("../../assets/images/add_free_experience.png")}
                style={styles.image}
                resizeMode="center"
              />
              <Text style={styles.headingText}>Enjoy Ad-Free Experience</Text>
              <Text style={styles.subHeadingText}>
                You're going to no adds and we mean it.
              </Text>
            </View>
          );
        }}
      />
      <Pagination scrollX={scrollX} items={items} />
    </View>
  );
};

export default ImageSlider;

ImageSlider.propTypes = {
  items: PropTypes.array.isRequired,
};

const styles = StyleSheet.create({
  imageContainer: {
    alignItems: "center",
  },
  pagination: {
    position: "absolute",
    bottom: 5,
    flexDirection: "row",
    height: DOT_SIZE,
    alignSelf: "center",
  },
  paginationDot: {
    width: DOT_SIZE * 0.5,
    height: DOT_SIZE * 0.5,
    borderRadius: DOT_SIZE * 0.25,
  },
  paginationDotContainer: {
    width: DOT_SIZE,
    alignItems: "center",
    justifyContent: "center",
  },
  paginationIndicator: {
    width: DOT_SIZE,
    height: DOT_SIZE,
    borderRadius: DOT_SIZE / 2,
    borderWidth: 2,
    borderColor: transparent.lightBlue,
  },
  image: {
    height: IMAGE_HEIGHT,
    width: IMAGE_WIDTH,
    alignSelf: "center",
  },
  headingText: {
    ...Typography.fontSize.x30,
    ...Typography.fontWeight.bold,
    marginBottom: Sizing.layout.x5,
  },
  subHeadingText: {
    ...Typography.fontSize.x10,
    color: colors.lightGrey,
    marginBottom: Sizing.layout.x40,
  },
});
