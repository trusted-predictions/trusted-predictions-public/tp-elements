import { Sizing, Typography } from ".";
import { layout } from "./sizing";
import { colors } from "./theme";
import { fontSize } from "./typography";

const cardHeaderContainer = {
  flexDirection: "row",
  justifyContent: "space-between",
  marginBottom: 8,
};

const cardContainer = { borderRadius: 8 };

const cardTitle = {
  ...fontSize.x30,
  ...Typography.fontWeight.bold,
  textTransform: "capitalize",
};

const cardRight = { paddingHorizontal: 8, paddingVertical: 4 };

const cardBorderRadius = {
  borderRadius: 16,
};

const redBorder = { borderColor: colors.danger, borderWidth: layout.x2 };

const cardRightText = { color: colors.lightGrey };

export const cardStyles = {
  cardHeaderContainer,
  cardContainer,
  cardTitle,
  cardRight,
  cardRightText,
  redBorder,
  cardBorderRadius,
};
