import { Sizing, Typography } from ".";

const container = {
  width: 75,
  height: 35,
  marginHorizontal: Sizing.layout.x4,
  marginTop: Sizing.layout.x8,
  borderRadius: 12,
  flexDirection: "row",
  justifyContent: "space-evenly",
  alignItems: "center",
  marginBottom: 12,
};

const dummyImage = {
  width: 20,
  height: 15,
  borderRadius: Sizing.layout.x5,
};

const title = { ...Typography.fontSize.x20, textTransform: "uppercase" };

export default { container, dummyImage, title };
