import { Dimensions } from "react-native";

const { width } = Dimensions.get("window");

export const bowlingTableWidth = {
  over: 63,
  small: 25,
  mid: 32,
  nameWidth: width - 56 - 170 - 12,
};

export const battingTableWidth = {
  runsWidth: 60,
  boundariesWidth: 30,
  strikeRateWidth: 50,
  nameWidth: width - 56 - 170 - 12,
};
