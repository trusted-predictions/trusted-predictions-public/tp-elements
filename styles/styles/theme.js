export const theme = {
  colors: {
    primary: '#0088FF',
    error: '#ff0000',
  },
};

export default theme;

// interface theme {
//   colors: {
//     primary;
//     secondary;
//     white;
//     black;
//     grey0;
//     grey1;
//     grey2;
//     grey3;
//     grey4;
//     grey5;
//     greyOutline;
//     searchBg;
//     success;
//     error;
//     warning;
//     divider;
//     platform: {
//       ios: {
//         primary;
//         secondary;
//         grey;
//         searchBg;
//         success;
//         error;
//         warning;
//       };
//       android: {
//         // Same as ios
//       };
//       web: {
//         // Same as ios
//       };
//     };
//   };
// }
