import { fontSize, fontWeight } from "./typography";

export const colors = {
  primary: "#0088FF",
  error: "#ff0000",
  lightGrey: "#6E6E6E",
  grey: "#5A5A5A",
  veryLightGrey: "#959A9E",
  inputBorder: "#808080",
  deepPink: "#EC6666",
  lightDark: "#000000D4",
  white: "#fff",
  danger: "#cf1717",
  black: "#000",
  success: "#14AE29",
  lightestGrey: "#E9E9E9",
  darkOverLay1: "rgba(0,0,0,.1)",
  yellow: "#FFFF00",
  textGrey: "#333333",
};

// interface theme {
//   colors: {
//     primary;
//     secondary;
//     white;
//     black;
//     grey0;
//     grey1;
//     grey2;
//     grey3;
//     grey4;
//     grey5;
//     greyOutline;
//     searchBg;
//     success;
//     error;
//     warning;
//     divider;
//     platform: {
//       ios: {
//         primary;
//         secondary;
//         grey;
//         searchBg;
//         success;
//         error;
//         warning;
//       };
//       android: {
//         // Same as ios
//       };
//       web: {
//         // Same as ios
//       };
//     };
//   };
// }

export default {
  colors,
  Input: {
    labelStyle: { ...fontWeight.semibold, color: colors.lightGrey },
    inputContainerStyle: { borderBottomColor: colors.veryLightGrey },
    inputStyle: fontSize.x25,
  },
  Divider: {
    backgroundColor: colors.lightestGrey,
  },
};
