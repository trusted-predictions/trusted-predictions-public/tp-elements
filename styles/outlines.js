export const borderRadius = {
  small: 5,
  base: 10,
  mid: 16,
  large: 20,
  max: 9999,
};

export const borderWidth = {
  thin: 1,
  base: 2,
  thick: 3,
};
