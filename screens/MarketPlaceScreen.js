import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { Button, Card, Divider } from "react-native-elements";
import { Button as PaperButton, TouchableRipple } from "react-native-paper";
import ImageSlider from "../Component/MarketPlace/ImageSlider";
import TwoTabs from "../Component/TwoTabs";
import { Sizing, Typography } from "../styles";
import { cardStyles } from "../styles/card";
import { percentage } from "../styles/sizing";
import { colors } from "../styles/theme";

const MarketPlaceScreen = () => {
  const buyCoins = () => {
    return (
      <>
        <Card containerStyle={cardStyles.cardBorderRadius}>
          <View style={{ flexDirection: "row", position: "relative" }}>
            <View>
              <Text
                style={{
                  ...Typography.fontSize.x50,
                  color: colors.danger,
                  ...Typography.fontWeight.bold,
                  textTransform: "uppercase",
                }}
              >
                get 20% free
              </Text>
              <Text numberOfLines={1} style={styles.secondaryText}>
                Offer only valid for today.
              </Text>
              <PaperButton
                onPress={() => {}}
                mode="contained"
                style={{
                  backgroundColor: colors.success,
                  flexDirection: "row",
                  marginTop: 18,
                }}
                uppercase={false}
              >
                <Text style={Typography.fontSize.x10}>
                  Rs 60{"  "}1000 +{" "}
                  <Text style={{ color: colors.yellow }}>200</Text> Coins
                </Text>
              </PaperButton>
            </View>

            <Image
              source={require("../assets/images/Coins-PNG-Photos.png")}
              style={styles.rupeeBagImage}
              resizeMode="contain"
            />
          </View>
        </Card>
        {[...Array(6).keys()].map((item) => (
          <Card
            key={item.toString()}
            wrapperStyle={styles.buyCoinsListItem}
            containerStyle={{ borderRadius: Sizing.layout.x8 }}
          >
            <View>
              <Text style={styles.buyCoinListText}>Basic Plan</Text>
              <Text style={[styles.buyCoinListText, { color: colors.success }]}>
                100-500 Coins
              </Text>
            </View>
            <TouchableRipple
              onPress={() => {}}
              style={styles.buyCoinListButton}
            >
              <Text style={styles.buyCoinListButtonText}>Rs 60</Text>
            </TouchableRipple>
          </Card>
        ))}
      </>
    );
  };

  const premiumTab = () => {
    return (
      <Card containerStyle={{ borderRadius: Sizing.layout.x20 }}>
        <ImageSlider items={[...Array(6).keys()]} />
        <Divider style={styles.divider} />
        <View style={styles.itemContainer}>
          <View style={{ flex: 5 }}>
            <Text style={styles.primaryText}>Free trail</Text>
            <Text numberOfLines={1} style={styles.secondaryText}>
              Experience premium for 7-Days
            </Text>
          </View>
          <Button
            containerStyle={styles.buttonStyle}
            title="Start 7-Days Trial"
            type="outline"
            onPress={() => {}}
            titleStyle={Typography.fontSize.x10}
          />
        </View>
        <View style={styles.itemContainer}>
          <View style={{ flex: 5 }}>
            <Text numberOfLines={1} style={styles.primaryText}>
              Monthly Subscription
            </Text>
            <Text style={styles.secondaryText} numberOfLines={1}>
              Experience premium for 7-Days
            </Text>
          </View>
          <Button
            containerStyle={styles.buttonStyle}
            title="Rs. 149 / Mouth"
            type="solid"
            buttonStyle={{ backgroundColor: colors.success }}
            onPress={() => {}}
            titleStyle={Typography.fontSize.x10}
          />
        </View>
        <View style={styles.itemContainer}>
          <View style={{ flex: 5 }}>
            <Text style={styles.primaryText}>Half Yearly Pack</Text>
            <Text numberOfLines={1} style={styles.secondaryText}>
              Most Popular Plan
            </Text>
          </View>
          <Button
            containerStyle={styles.buttonStyle}
            title="Rs. 600 / Half-year"
            type="solid"
            onPress={() => {}}
            titleStyle={Typography.fontSize.x10}
            buttonStyle={{ backgroundColor: colors.danger }}
          />
        </View>
        <View style={styles.itemContainer}>
          <View style={{ flex: 5 }}>
            <Text style={styles.primaryText}>Annual Subscription</Text>
            <Text style={styles.secondaryText} numberOfLines={1}>
              Pay once and enjoy premium for a year
            </Text>
          </View>
          <Button
            containerStyle={styles.buttonStyle}
            title="Rs 1000 / year"
            type="solid"
            onPress={() => {}}
            buttonStyle={{ backgroundColor: colors.primary }}
            titleStyle={Typography.fontSize.x10}
          />
        </View>
      </Card>
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.lightestGrey,
      }}
    >
      <TwoTabs
        tab1name="Buy Coins"
        tab1={buyCoins}
        tab2name="Premium"
        tab2={premiumTab}
      />
    </View>
  );
};

export default MarketPlaceScreen;

const styles = StyleSheet.create({
  itemContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: Sizing.layout.x10,
  },
  primaryText: {
    ...Typography.fontSize.x15,
    ...Typography.fontWeight.semibold,
    textTransform: "capitalize",
  },
  secondaryText: {
    ...Typography.fontSize.x10,
    color: colors.veryLightGrey,
  },
  divider: {
    backgroundColor: colors.lightestGrey,
    marginVertical: Sizing.layout.x4,
  },
  buttonStyle: {
    ...percentage.w30,
  },
  buyCoinListButton: {
    backgroundColor: colors.primary,
    paddingHorizontal: Sizing.layout.x30,
    paddingVertical: Sizing.layout.x5,
    borderRadius: Sizing.layout.x5,
  },
  buyCoinListButtonText: { color: colors.white, ...Typography.fontSize.x10 },
  buyCoinListText: { color: colors.lightGrey, ...Typography.fontSize.x15 },
  buyCoinsListItem: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  rupeeBagImage: {
    height: 95,
    width: 95,
    position: "absolute",
    right: 0,
    top: 0,
  },
});
