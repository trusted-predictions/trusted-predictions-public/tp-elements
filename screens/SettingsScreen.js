import React from "react";
import { TouchableOpacity } from "react-native";
import { StyleSheet, Text, View } from "react-native";
import { Card, Icon } from "react-native-elements";
import { Sizing, Typography } from "../styles";
import { icons } from "../styles/sizing";
import { colors } from "../styles/theme";

const SettingsScreen = () => {
  const settingsItem = [
    {
      iconName: "settings-sharp",
      iconType: "ionicon",
      title: " General Settings",
      id: "1",
      onPress: () => {},
    },
    {
      iconName: "shield-alt",
      iconType: "font-awesome-5",
      title: " Change Password",
      id: "2",
      onPress: () => {},
    },
  ];

  return (
    <View style={{ flex: 1, backgroundColor: colors.lightestGrey }}>
      <Card containerStyle={styles.card}>
        {settingsItem.map((item) => (
          <TouchableOpacity
            key={item.id}
            activeOpacity={0.7}
            onPress={item.onPress}
            style={styles.item}
          >
            <Icon
              type={item.iconType}
              name={item.iconName}
              color={colors.veryLightGrey}
              style={{ marginRight: Sizing.layout.x12 }}
              size={icons.x20}
            />
            <Text style={styles.text}>{item.title}</Text>
          </TouchableOpacity>
        ))}
      </Card>
    </View>
  );
};

export default SettingsScreen;

const styles = StyleSheet.create({
  card: {
    borderRadius: Sizing.layout.x16,
    flexGrow: 1,
    marginBottom: Sizing.layout.x12,
  },
  item: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: Sizing.layout.x8,
    marginVertical: Sizing.layout.x2,
  },
  text: {
    ...Typography.fontSize.x25,
    color: colors.textGrey,
    textTransform: "capitalize",
  },
});
