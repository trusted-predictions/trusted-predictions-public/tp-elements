import React from "react";
import { Dimensions } from "react-native";
import { StyleSheet } from "react-native";
import LiveMatches from "../Component/LiveMatches";
import ScrollableTabs from "../Component/ScrollableTabs";
import UpcomingMatches from "../Component/UpcomingMatches";

const { width } = Dimensions.get("window");

const MatchesScreen = () => {
  const tabs = [
    {
      title: "Live",
      tab: () => (
        <>
          <LiveMatches />
          <UpcomingMatches />
        </>
      ),
    },
    {
      title: "Upcoming",
      tab: () => (
        <>
          <LiveMatches />
          <UpcomingMatches />
        </>
      ),
    },
    {
      title: "Completed",
      tab: () => (
        <>
          <LiveMatches />
          <UpcomingMatches />
        </>
      ),
    },
  ];

  return <ScrollableTabs tabs={tabs} />;
};

export default MatchesScreen;

const styles = StyleSheet.create({});
