import React from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import DateSlider from "../Component/Predections/DateSlider";
import FantasyTeamList from "../Component/Predections/FantasyTeams";
import MatchesSlider from "../Component/Predections/MatchesSlider";
import Playing11card from "../Component/Predections/Playing11card";
import TopPerformersCard from "../Component/Predections/TopPerformersCard";
import WhoWillWin from "../Component/Predections/WhoWillWin";

const dates = [
  "20 jul",
  "21 jul",
  "22 aug",
  "23 Sept",
  "25 Sept",
  "26 Sept",
  "27th oct",
];

const PredictionsScreen = () => {
  return (
    <View style={{ alignItems: "center", backgroundColor: "#ECECEC", flex: 1 }}>
      <ScrollView>
        <DateSlider dates={dates} />
        <MatchesSlider items={[...Array(7).keys()]} />
        <WhoWillWin />
        <Playing11card />
        <TopPerformersCard />
        <FantasyTeamList />
      </ScrollView>
    </View>
  );
};

export default PredictionsScreen;

const styles = StyleSheet.create({});
