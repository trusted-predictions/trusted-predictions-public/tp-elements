import React from "react";
import { StyleSheet, Text, View } from "react-native";
import ScrollableTabs from "../Component/ScrollableTabs";
import Matches from "../Component/SeriesDetails/Matches";
import News from "../Component/SeriesDetails/News";
import PointsTable from "../Component/SeriesDetails/PointsTable";
import Squads from "../Component/SeriesDetails/Squads";
import Stats from "../Component/SeriesDetails/Stats";

const SeriesDetailsScreen = () => {
  const tabs = [
    {
      title: "Matches",
      tab: () => <Matches />,
    },
    {
      title: "Points Table",
      tab: () => <PointsTable />,
    },
    {
      title: "Stats",
      tab: () => <Stats />,
    },
    {
      title: "Squads",
      tab: () => <Squads />,
    },
    {
      title: "News",
      tab: () => <News />,
    },
  ];

  return (
    <ScrollableTabs
      tabs={tabs}
      headerTitle="IPL 2020"
      scrollable
      scrollEnabled={false}
    />
  );
};

export default SeriesDetailsScreen;

const styles = StyleSheet.create({});
