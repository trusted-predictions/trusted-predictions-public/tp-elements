import React from "react";
import { ScrollView } from "react-native";
import { StyleSheet, Text, View } from "react-native";
import { ListItem, Icon } from "react-native-elements";
import { Sizing, Typography } from "../styles";
import { icons } from "../styles/sizing";
import { colors } from "../styles/theme";

const MyListItem = ({ item }) => (
  <ListItem activeOpacity={0.96} bottomDivider onPress={item.onPress}>
    <Icon
      type={item.type}
      name={item.name}
      color={colors.primary}
      size={item.size || icons.x20}
      style={{ marginHorizontal: item.size ? 2 : 0 }}
    />
    <ListItem.Content>
      <ListItem.Title style={styles.titleStyle}>{item.title}</ListItem.Title>
    </ListItem.Content>
    <Icon
      type="entypo"
      size={icons.x20}
      name="chevron-right"
      color={colors.lightGrey}
    />
  </ListItem>
);

const MoreScreens = () => {
  const listItemsOne = [
    {
      title: "series",
      onPress: () => {},
      type: "material",
      name: "radio-button-on",
    },
  ];

  const listItemTwo = [
    {
      title: "player",
      onPress: () => {},
      type: "font-awesome-5",
      name: "user-alt",
      size: 16,
    },
    { title: "teams", onPress: () => {}, type: "material", name: "group" },
    {
      title: "stadiums",
      onPress: () => {},
      type: "material-community",
      name: "stadium",
    },
  ];
  const listItemFour = [
    {
      title: "Rankings",
      onPress: () => {},
      type: "material",
      name: "bar-chart",
    },
    {
      title: "Records",
      onPress: () => {},
      type: "material-community",
      name: "record-circle",
    },
    {
      title: "Stat Attack",
      onPress: () => {},
      type: "entypo",
      name: "documents",
    },
  ];
  const listItemFive = [
    {
      title: "settings",
      onPress: () => {},
      name: "settings-sharp",
      type: "ionicon",
    },
    {
      title: "profile",
      onPress: () => {},
      type: "font-awesome",
      name: "user-circle-o",
    },
  ];

  const listItemsThree = [
    {
      title: "videos",
      onPress: () => {},
      type: "ionicon",
      name: "play-circle",
    },
  ];

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      bounces={false}
      style={{ backgroundColor: colors.lightestGrey }}
      contentContainerStyle={{ flexGrow: 1 }}
    >
      <View style={{ height: 6 }} />
      <View style={styles.listGroup}>
        {listItemsOne.map((item) => (
          <MyListItem key={item.name} item={item} />
        ))}
      </View>
      <View style={styles.listGroup}>
        {listItemTwo.map((item) => (
          <MyListItem key={item.name} item={item} />
        ))}
      </View>
      <View style={styles.listGroup}>
        {listItemsThree.map((item) => (
          <MyListItem key={item.name} item={item} />
        ))}
      </View>
      <View style={styles.listGroup}>
        {listItemFour.map((item) => (
          <MyListItem key={item.name} item={item} />
        ))}
      </View>
      <View style={styles.listGroup}>
        {listItemFive.map((item) => (
          <MyListItem key={item.name} item={item} />
        ))}
      </View>
      <View style={{ height: 12 }} />
    </ScrollView>
  );
};

export default MoreScreens;

const styles = StyleSheet.create({
  titleStyle: {
    ...Typography.fontSize.x30,
    textTransform: "capitalize",
  },
  listGroup: {
    marginVertical: Sizing.layout.x5,
  },
});
