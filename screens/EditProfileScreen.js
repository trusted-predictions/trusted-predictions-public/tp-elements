import React, { useState } from "react";
import { Image } from "react-native";
import { ScrollView } from "react-native";
import { StyleSheet, Text, View } from "react-native";
import { Button, Card, Divider, Icon, Input } from "react-native-elements";
import { Sizing, Typography } from "../styles";
import { icons } from "../styles/sizing";
import { colors } from "../styles/theme";

const EditProfileScreen = ({ navigation }) => {
  const [firstName, setFirstName] = useState("supriyo");
  const [lastName, setLastName] = useState("mondal");
  const [username, setUsername] = useState("sup@07");
  const [email, setEmail] = useState("supriyo@example.com");

  const handelSave = () => {
    navigation.goBack();
  };

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      bounces={false}
      style={{ backgroundColor: colors.lightestGrey }}
      contentContainerStyle={{ flexGrow: 1 }}
    >
      <Card containerStyle={styles.card}>
        <View style={{ flexDirection: "row", alignItems: "center" }}>
          <Image
            source={require("../assets/images/user.png")}
            style={{ flex: 1, height: 100 }}
            resizeMode="contain"
          />
          <View style={{ flex: 2, marginLeft: Sizing.layout.x16 }}>
            <Text style={styles.primaryText}>Supriyo Mondal</Text>
            <Text style={styles.secondaryText}>{username}</Text>
          </View>
        </View>
        <Divider style={{ marginVertical: Sizing.layout.x16 }} />
        <Input
          label="First Name"
          placeholder="Enter first name"
          value={firstName}
          style={styles.input}
          onChangeText={(text) => setFirstName(text)}
          leftIcon={() => (
            <Icon
              type="font-awesome-5"
              name="user-alt"
              color={colors.primary}
              size={18}
            />
          )}
        />
        <Input
          label="Last Name"
          placeholder="Enter last name"
          value={lastName}
          style={styles.input}
          onChangeText={(text) => setLastName(text)}
          leftIcon={() => (
            <Icon
              type="font-awesome-5"
              name="user-alt"
              color={colors.primary}
              size={18}
            />
          )}
        />
        <Input
          label="Username"
          disabled
          placeholder="Enter username"
          value={firstName}
          style={styles.input}
          leftIcon={() => (
            <Icon
              type="ionicon"
              name="mail"
              color={colors.veryLightGrey}
              size={18}
            />
          )}
        />

        <Input
          label="Email"
          placeholder="Enter email"
          value={email}
          style={styles.input}
          onChangeText={(text) => setEmail(text)}
          leftIcon={() => (
            <Icon
              type="ionicon"
              name="mail"
              color={colors.veryLightGrey}
              size={18}
            />
          )}
        />
      </Card>
      <Button
        title="Save"
        onPress={handelSave}
        titleStyle={Typography.fontSize.x25}
        buttonStyle={styles.btn}
        containerStyle={{ marginVertical: Sizing.layout.x12 }}
      />
    </ScrollView>
  );
};

export default EditProfileScreen;

const styles = StyleSheet.create({
  card: {
    borderRadius: Sizing.layout.x16,
    flexGrow: 1,
  },
  btn: {
    marginHorizontal: Sizing.layout.x12,
    borderRadius: Sizing.layout.x10,
  },
  primaryText: {
    ...Typography.fontSize.x40,
    ...Typography.fontWeight.bold,
    textTransform: "capitalize",
  },
  secondaryText: {
    color: colors.lightGrey,
    marginTop: Sizing.layout.x2,
    ...Typography.fontSize.x15,
  },
  input: { paddingLeft: 8 },
});
