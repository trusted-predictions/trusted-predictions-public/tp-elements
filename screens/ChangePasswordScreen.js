import React, { useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Button, Card, Input, Icon } from "react-native-elements";
import { Sizing, Typography } from "../styles";
import { icons } from "../styles/sizing";
import { colors } from "../styles/theme";

const ChangePasswordScreen = () => {
  const [oldPassword, setOldPassword] = useState("password");
  const [oldPasswordSecure, setOldPasswordSecure] = useState(true);
  const [newPassword, setNewPassword] = useState("");
  const [repeatNewPassword, setRepeatNewPassword] = useState("");
  const [newPasswordSecure, setNewPasswordSecure] = useState(false);
  const [repeatNewPasswordSecure, setRepeatNewPasswordSecure] = useState(false);
  return (
    <View style={{ flex: 1, backgroundColor: colors.lightestGrey }}>
      <Card containerStyle={styles.card}>
        <Input
          placeholder="Enter old password"
          value={oldPassword}
          disabled
          secureTextEntry={oldPasswordSecure}
          rightIcon={() => (
            <Icon
              name={oldPasswordSecure ? "eye-off" : "eye"}
              type="ionicon"
              onPress={() => setOldPasswordSecure(!oldPasswordSecure)}
              size={icons.x20}
              color={oldPasswordSecure ? colors.veryLightGrey : colors.primary}
            />
          )}
          label="Old password"
        />
        <Input
          placeholder="Enter new password"
          placeholderTextColor={colors.veryLightGrey}
          value={newPassword}
          secureTextEntry={newPasswordSecure}
          onChangeText={(text) => setNewPassword(text)}
          rightIcon={() => (
            <Icon
              name={newPasswordSecure ? "eye-off" : "eye"}
              type="ionicon"
              onPress={() => setNewPasswordSecure(!newPasswordSecure)}
              size={icons.x20}
              color={newPasswordSecure ? colors.veryLightGrey : colors.primary}
            />
          )}
          label="New password"
        />
        <Input
          placeholder="Enter new password"
          onChangeText={(text) => setRepeatNewPassword(text)}
          value={repeatNewPassword}
          secureTextEntry={repeatNewPasswordSecure}
          rightIcon={() => (
            <Icon
              name={repeatNewPasswordSecure ? "eye-off" : "eye"}
              type="ionicon"
              onPress={() =>
                setRepeatNewPasswordSecure(!repeatNewPasswordSecure)
              }
              size={icons.x20}
              color={
                repeatNewPasswordSecure ? colors.veryLightGrey : colors.primary
              }
            />
          )}
          label="Old password"
        />
      </Card>
      <Button
        title="Save"
        onPress={() => {}}
        titleStyle={Typography.fontSize.x25}
        buttonStyle={styles.btn}
        containerStyle={{ marginVertical: Sizing.layout.x12 }}
      />
    </View>
  );
};

export default ChangePasswordScreen;

const styles = StyleSheet.create({
  card: {
    borderRadius: Sizing.layout.x16,
    flexGrow: 1,
  },
  btn: {
    marginHorizontal: Sizing.layout.x12,
    borderRadius: Sizing.layout.x10,
  },
});
