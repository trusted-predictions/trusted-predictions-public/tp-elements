import React from "react";
import {
  SafeAreaView,
  View,
  FlatList,
  StyleSheet,
  Text,
  StatusBar,
} from "react-native";
import { Button } from "react-native-elements";
import Routes from "../navigation/Routes";

const DATA = [
  {
    id: "bd7acbea-c1b1-46c2-aed5-3ad53abb28ba",
    title: "Home Page",
    link: Routes.HOME,
  },
  {
    id: "2",
    title: "Marketplace",
    link: Routes.MARKETPLACE,
  },
  {
    id: "3",
    title: "Matches",
    link: Routes.MATCHES,
  },
  {
    id: "4",
    title: "Predictions",
    link: Routes.PREDICTIONS,
  },
  {
    id: "5",
    title: "Series Details",
    link: Routes.SERIES_DETAILS,
  },
  {
    id: "6",
    title: "Settings",
    link: Routes.SETTINGS,
  },
  {
    id: "7",
    title: "Match Details",
    link: Routes.MATCH_DETAILS,
  },
  {
    id: "8",
    title: "Change Password",
    link: Routes.CHANGE_PASSWORD,
  },
  {
    id: "9",
    title: "Edit Profile",
    link: Routes.EDIT_PROFILE,
  },
  {
    id: "10",
    title: "More",
    link: Routes.MORE,
  },
  {
    id: "11",
    title: "Drawer",
    link: Routes.DRAWER,
  },
];

const Item = ({ title, link, navigation }) => (
  <View style={styles.item}>
    <Text style={styles.title}>{title}</Text>
    <Button title="Go to Details" onPress={() => navigation.navigate(link)} />
  </View>
);

const PageListScreen = ({ navigation }) => {
  const renderItem = ({ item }) => (
    <Item title={item.title} link={item.link} navigation={navigation} />
  );

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: "#f9c2ff",
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default PageListScreen;
