import React from "react";
import { Dimensions } from "react-native";
import { Image } from "react-native";
import { StyleSheet, Text, View } from "react-native";
import LiveIcon from "../Component/Card/LiveIcon";
import FSPTams from "../Component/MatchDetailsScreen/FSPTams";
import Live from "../Component/MatchDetailsScreen/Live";
import Predictions from "../Component/MatchDetailsScreen/Predictions";
import Scorecard from "../Component/MatchDetailsScreen/Scorecard";
import ScrollableTabs from "../Component/ScrollableTabs";
import { Sizing, Typography } from "../styles";
import { colors } from "../styles/theme";

const { width } = Dimensions.get("window");

const MatchDetailsScreen = () => {
  const tabs = [
    {
      title: "Live",
      tab: () => <Live />,
    },
    {
      title: "Scorecard",
      tab: () => <Scorecard />,
    },
    {
      title: "FSP Teams",
      tab: () => <FSPTams />,
    },
    {
      title: "Predictions",
      tab: () => <Predictions />,
    },
  ];

  const tabHeader = () => (
    <View style={styles.header}>
      <LiveIcon style={styles.liveIcon} />
      <Text style={styles.secondaryText}>1st Innings</Text>
      <View style={styles.headerInside}>
        <View style={styles.teamNameAndLogo}>
          <Image
            source={require("../assets/images/Cricket_Australia_Logo.png")}
            style={styles.teamLogo}
            resizeMode="contain"
          />
          <Text adjustsFontSizeToFit numberOfLines={1} style={styles.teamName}>
            Australia
          </Text>
        </View>
        <Text adjustsFontSizeToFit numberOfLines={1} style={styles.scoreText}>
          -----
        </Text>
        <Text style={styles.score}>38.5 Overs</Text>
        <Text adjustsFontSizeToFit numberOfLines={1} style={styles.scoreText}>
          276-5
        </Text>
        <View style={styles.teamNameAndLogo}>
          <Image
            source={require("../assets/images/India.png")}
            style={styles.teamLogo}
            resizeMode="contain"
          />
          <Text adjustsFontSizeToFit numberOfLines={1} style={styles.teamName}>
            India
          </Text>
        </View>
      </View>
    </View>
  );

  return <ScrollableTabs tabs={tabs} header={tabHeader} />;
};

export default MatchDetailsScreen;

const styles = StyleSheet.create({
  header: {
    backgroundColor: colors.white,
    width,
    padding: Sizing.layout.x8,
    position: "relative",
    marginTop: Sizing.layout.x8,
  },
  liveIcon: {
    position: "absolute",
    top: Sizing.layout.x8,
    left: Sizing.layout.x8,
  },
  secondaryText: {
    textAlign: "center",
    ...Typography.fontSize.x20,
    color: colors.veryLightGrey,
    marginBottom: Sizing.layout.x8,
  },
  teamName: {
    ...Typography.fontSize.x15,
    textTransform: "uppercase",
    letterSpacing: 0.5,
    marginTop: Sizing.layout.x4,
  },
  teamLogo: { height: 50, width: 40 },
  teamNameAndLogo: { alignItems: "center", width: 80 },
  scoreText: {
    width: 45,
    ...Typography.fontSize.x20,
    textAlign: "center",
    color: colors.lightGrey,
  },
  headerInside: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginVertical: Sizing.layout.x12,
  },
  score: {
    color: colors.danger,
    marginHorizontal: 8,
  },
});
