import React from "react";
import { ScrollView, SafeAreaView } from "react-native";
import ImageSlider from "../Component/ImageSlider";
import LiveMatches from "../Component/LiveMatches";
import ReferPage from "../Component/CoinManagement/ReferPage";
import UpcomingMatches from "../Component/UpcomingMatches";

const sliderImages = [
  {
    id: "1",
    image:
      "https://images.unsplash.com/photo-1583430164827-6f9e5a68806d?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDEyfHx8ZW58MHx8fA%3D%3D&auto=format&fit=crop&w=400&q=60",
  },
  {
    id: "2",
    image:
      "https://images.unsplash.com/photo-1586333097369-4cb2d7bbf5a2?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDd8fHxlbnwwfHx8&auto=format&fit=crop&w=400&q=60",
  },
  {
    id: "3",
    image:
      "https://images.unsplash.com/photo-1592133589234-375b5d93d9c9?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDE0fHx8ZW58MHx8fA%3D%3D&auto=format&fit=crop&w=400&q=60",
  },
];

const HomeScreen = () => {
  return (
    <SafeAreaView
      style={{ flex: 1, alignItems: "center", backgroundColor: "#F5F3F3" }}
    >
      <ScrollView>
        <ImageSlider items={sliderImages} />
        <LiveMatches />
        <UpcomingMatches />
        <ReferPage />
      </ScrollView>
    </SafeAreaView>
  );
};

export default HomeScreen;
