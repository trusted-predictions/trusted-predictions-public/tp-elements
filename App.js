// In App.js in a new project

import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import PageListScreen from "./screens/PageListScreen";
import HomeScreen from "./screens/HomeScreen";
import Routes from "./navigation/Routes";
import MarketPlaceScreen from "./screens/MarketPlaceScreen";
import MatchesScreen from "./screens/MatchesScreen";
import PredictionsScreen from "./screens/PredictionsScreen";
import SeriesDetailsScreen from "./screens/SeriesDetailsScreen";
import SettingsScreen from "./screens/SettingsScreen";
import MatchDetailsScreen from "./screens/MatchDetailsScreen";
import ChangePasswordScreen from "./screens/ChangePasswordScreen";
import { ThemeProvider } from "react-native-elements";
import theme from "./styles/theme";
import EditProfileScreen from "./screens/EditProfileScreen";
import MoreScreens from "./screens/MoreScreens";
import Drawer from "./Component/Drawer";

const Stack = createStackNavigator();

function App() {
  return (
    <ThemeProvider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName={Routes.PAGE_LIST}>
          <Stack.Screen name={Routes.PAGE_LIST} component={PageListScreen} />
          <Stack.Screen name={Routes.HOME} component={HomeScreen} />
          <Stack.Screen
            name={Routes.MARKETPLACE}
            component={MarketPlaceScreen}
          />
          <Stack.Screen name={Routes.MATCHES} component={MatchesScreen} />
          <Stack.Screen
            name={Routes.PREDICTIONS}
            component={PredictionsScreen}
          />
          <Stack.Screen
            name={Routes.SERIES_DETAILS}
            component={SeriesDetailsScreen}
          />
          <Stack.Screen name={Routes.SETTINGS} component={SettingsScreen} />
          <Stack.Screen
            name={Routes.MATCH_DETAILS}
            component={MatchDetailsScreen}
          />
          <Stack.Screen
            name={Routes.CHANGE_PASSWORD}
            component={ChangePasswordScreen}
          />
          <Stack.Screen
            name={Routes.EDIT_PROFILE}
            component={EditProfileScreen}
          />
          <Stack.Screen name={Routes.MORE} component={MoreScreens} />
          <Stack.Screen name={Routes.DRAWER} component={Drawer} />
        </Stack.Navigator>
      </NavigationContainer>
    </ThemeProvider>
  );
}

export default App;
